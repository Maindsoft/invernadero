﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Errores_Error404 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        { 
            
        }
    }

    protected void Button_Retroceso_OnClick(object sender, EventArgs e)
    {
        this.Response.Redirect(SystemConstantes.WEB_FORM__LOGIN.UrlCompleta);
    }
}