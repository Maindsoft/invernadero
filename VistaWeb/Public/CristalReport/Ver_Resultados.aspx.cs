﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using CrystalDecisions.Shared;
//using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class Estudios_VerResultados : System.Web.UI.Page
{
    private const string RUTA_GUARDADO_REPORTES = @"\Estudios\Reportes\";
    //CrystalDecisions.CrystalReports.Engine.ReportDocument reportdocument = new CrystalDecisions.CrystalReports.Engine.ReportDocument();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (UsuarioStatic.getObj(this) == null)
        {
            // No tiene ningún acceso
            this.Response.Redirect(SystemConstantes.WEB_FORM__SIN_ACCESO.UrlCompleta);
        }

        string layout = "";
        TSHAK.Components.SecureQueryString querystringSeguro;        // instanciamos el objeto y le pasamos como argumento el mismo array 'de bits mas el parámetro data, que viene de la llamada de la 'pagina default.aspx que contiene todo el queryString
        querystringSeguro = new TSHAK.Components.SecureQueryString(new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 8 }, Request["p"]);
        try
        {
            LabelId.Text = querystringSeguro["id_estudio"];

            layout = querystringSeguro["layout"];
            LabelLayOut.Text = layout;
        }
        catch (Exception ex)
        {
        }

        //ConnectionInfo iConnectionInfo = new ConnectionInfo();

        //iConnectionInfo.DatabaseName = DataAccess.Util.getNombreBaseDatos();
        //iConnectionInfo.UserID = DataAccess.Util.getUsuario();
        //iConnectionInfo.Password = DataAccess.Util.getcontraseña();
        //iConnectionInfo.ServerName = DataAccess.Util.getServerName();
        //iConnectionInfo.IntegratedSecurity = DataAccess.Util.getIntegratedSecurity();


        //iConnectionInfo.Type = ConnectionInfoType.SQL;


        //string tipoReporte = Core.Estudios.GestionEstudios.ObtenerTipoReporte(LabelId.Text, UsuarioStatic.getId(this));
        
        
        //reportdocument.Load(Server.MapPath("~/Public/CristalReport/"+tipoReporte));
       

        //SetDBLogonForReport(iConnectionInfo, reportdocument);

        //Resultado.ReportSource = reportdocument;
        //if (tipoReporte != "ResultadoCitologiaTipificacion.rpt")
        //{
        //    reportdocument.SetParameterValue("ID_ESTUDIO", LabelId.Text);
        //    reportdocument.SetParameterValue("ID_ESTUDIO2", LabelId.Text);
        //}
        //else {
        //    reportdocument.SetParameterValue("ID_ESTUDIO", LabelId.Text);
            
        //}

        //string nombrePaciente = BLL.MulData.SelectCondicion(" P.NOMBRE from ESTUDIOS E INNER JOIN CLIENTES P ON E.ID_PACIENTE = P.ID_CLIENTE WHERE ID_ESTUDIO = " + LabelId.Text).Rows[0]["NOMBRE"].ToString();

        //string clave = BLL.MulData.SelectCondicion("CLAVE_ESTUDIO from ESTUDIOS where ID_ESTUDIO = " + LabelId.Text).Rows[0]["CLAVE_ESTUDIO"].ToString();

        //// SELECCIONA LA RUTA DEL SISTEMA
        //string currentPath = HttpContext.Current.Server.MapPath("");
        //// RUTA SEGUN LA FECHA Y EL RECEPTOR

        //// SE ESPECIFICA LA RUTA DONDE SE VA A GUARDAR EL XML
        //string resultPath = currentPath + RUTA_GUARDADO_REPORTES;
        //if (!Directory.Exists(resultPath))
        //    Directory.CreateDirectory(resultPath);
        //reportdocument.ExportToDisk(ExportFormatType.PortableDocFormat, (resultPath + Convert.ToString(@"\")) +nombrePaciente+" "+ clave + ".pdf");

        //reportdocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true,nombrePaciente+" "+ clave);

        //Response.End();
        
        

    }

    //private void SetDBLogonForReport(ConnectionInfo myConnectionInfo, ReportDocument myReportDocument)
    //{
    //    Tables myTables = myReportDocument.Database.Tables;
    //    foreach (CrystalDecisions.CrystalReports.Engine.Table myTable in myTables)
    //    {
    //        TableLogOnInfo myTableLogonInfo = myTable.LogOnInfo;
    //        myTableLogonInfo.ConnectionInfo = myConnectionInfo;
    //        myTable.ApplyLogOnInfo(myTableLogonInfo);
    //    }
    //}

    //protected void Page_Unload(object sender, System.EventArgs e)
    //{
    //    reportdocument.Close();
    //    reportdocument.Dispose();
    //}
}