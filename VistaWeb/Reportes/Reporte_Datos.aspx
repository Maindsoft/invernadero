﻿<%@ Page Title="Reporte Datos Día" Language="C#" MasterPageFile="~/Layout.master" AutoEventWireup="true" CodeFile="Reporte_Datos.aspx.cs" Inherits="Reportes_Reporte_Datos" %>
<%@ Register Assembly="DevExpress.XtraCharts.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="DevExpress.XtraCharts.v16.1.Web, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v16.1.Web, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxcharts" %>
<%@ Register Assembly="DevExpress.XtraCharts.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Principal" Runat="Server">
    <script type="text/javascript">

        function CambiarTabReportesDia() {
            document.getElementById('<%=LinkButton_TabReportesDia_Actualizar.ClientID%>').click();
        }


    </script>

    <!-- UPDATEPROGRESS START -->
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div class="image-loading-container">
                <asp:Image ID="imgUpdateProgress" runat="server" CssClass="image-loading" ImageUrl="~/Public/Images/load.gif"
                    AlternateText="Loading ..." ToolTip="Loading ..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <!-- UPDATEPROGRESS END -->
    <!-- CONTAINER START -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <!-- NAV START -->
                <ul class="nav u-nav-v5-1" role="tablist" data-target="nav-5-1-default-hor-left-icons"
                    data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-lightgray">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#TabReportesDia"
                        role="tab" onclick="CambiarTabReportesDia()"><i class="et-icon-clipboard u-tab-line-icon-pro g-mr-3">
                        </i>Reporte Datos </a></li>
                </ul>
                <!-- NAV END -->
                <!-- TAB PANES START -->
                <div id="nav-5-1-default-hor-left-icons" class="tab-content g-pt-20">
                    <!-- PESTAÑA CAPTURA DE USUARIOS START -->
                    <div class="tab-pane fade show active" id="TabReportesDia" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanel_TabReportesDia" runat="server" ChildrenAsTriggers="false"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Reporte de Datos</h3>
                                        <asp:LinkButton ID="LinkButton_TabReportesDia_Actualizar" OnClick="LinkButton_TabReportesDia_Actualizar_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-teal g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Actualizar</span>
                                        </asp:LinkButton>
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">
                                            <div class="form-group">
                                                <div class="form-group row">
                                                    <div class="col-3">
                                                        <dx:ASPxDateEdit ID="DateInicio" Theme="Moderno" Width="100%" UseMaskBehavior="true"
                                                        runat="server" DisplayFormatString="dd/MM/yyyy" EditFormat="Custom" EditFormatString="dd/MM/yyyy"></dx:ASPxDateEdit>
                                                    </div>
                                                    <div class="col-3">
                                                        <dx:ASPxDateEdit ID="DateFin" Theme="Moderno" Width="100%" UseMaskBehavior="true"
                                                        runat="server" DisplayFormatString="dd/MM/yyyy" EditFormat="Custom" EditFormatString="dd/MM/yyyy"></dx:ASPxDateEdit>
                                                    </div>
                                                    <div class="col-3">
                                                            <asp:Button ID="ButtonActualizar"  data-toggle="tooltip"
                                                        title="Actualiza" data-placement="bottom" runat="server" class="btn btn-info"
                                                        Text="Actualizar" Style="width: 100%;" onclick="ButtonActualizar_Click" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <dx:ASPxGridView ID="ASPxGridView_DatosSensores"  OnDataBinding="ASPxGridView_DatosSensores_OnDataBinding" Theme="Moderno" runat="server" AutoGenerateColumns="False" KeyFieldName="ID" Width="100%">
                                                             
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn FieldName="ALIAS"       Width="25%"         Caption="Sensor"       Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" ><CellStyle HorizontalAlign="Center"></CellStyle></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="NOMBRE"      Width="25%"         Caption="Tipo de lectura "  Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" ></dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DATO"        Width="25%"         Caption="Dato"       Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" />
                                                                <dx:GridViewDataDateColumn FieldName="FECHA_HORA"  Width="25%"  Caption="Fecha Hora Captura"    Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true">
                                                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:mm:ss" EditFormatString="dd/MM/yyyy hh:mm:ss"></PropertiesDateEdit>
                                                                </dx:GridViewDataDateColumn>
                                                            </Columns>
                                                            <Settings ShowFilterRow="True" AutoFilterCondition="Contains"  HorizontalScrollBarMode="Auto"  VerticalScrollableHeight="400" ShowFooter="true" />
                                                            
                                                            <SettingsPager PageSize="20" ShowNumericButtons="false"  EnableAdaptivity="true"/>
                                                            <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                           
                                                            <Styles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small" />
                                                                <Header Wrap="True" Font-Size="Small" />
                                                            </Styles>
                                                        </dx:ASPxGridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


