﻿<%@ Page Title="Reporte Datos Día" Language="C#" MasterPageFile="~/Layout.master" AutoEventWireup="true" CodeFile="Reporte_Datos_Dia.aspx.cs" Inherits="Reportes_Reporte_Datos_Dia" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.XtraCharts.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="DevExpress.XtraCharts.v16.1.Web, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v16.1.Web, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxcharts" %>
<%@ Register Assembly="DevExpress.XtraCharts.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Principal" Runat="Server">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        function CambiarTabReportesDia() {
          
        }
    </script>

    <!-- UPDATEPROGRESS START -->
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div class="image-loading-container">
                <asp:Image ID="imgUpdateProgress" runat="server" CssClass="image-loading" ImageUrl="~/Public/Images/load.gif"
                    AlternateText="Loading ..." ToolTip="Loading ..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <!-- UPDATEPROGRESS END -->
    <!-- CONTAINER START -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <!-- NAV START -->
                <ul class="nav u-nav-v5-1" role="tablist" data-target="nav-5-1-default-hor-left-icons"
                    data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-lightgray">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#TabReportesDia"
                        role="tab" onclick="CambiarTabReportesDia()"><i class="et-icon-clipboard u-tab-line-icon-pro g-mr-3">
                        </i>Reporte Datos Día </a></li>
                </ul>
                <!-- NAV END -->
                <!-- TAB PANES START -->
                <div id="nav-5-1-default-hor-left-icons" class="tab-content g-pt-20">
                    <!-- PESTAÑA CAPTURA DE USUARIOS START -->
                    <div class="tab-pane fade show active" id="TabReportesDia" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanel_TabReportesDia" runat="server" ChildrenAsTriggers="false"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Reporte de Datos Por Día</h3>
                                        <asp:LinkButton ID="LinkButton_TabReportesDia_Actualizar" OnClick="LinkButton_TabReportesDia_Actualizar_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-teal g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Actualizar</span>
                                        </asp:LinkButton>
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Invernadero: </label>
                                                        
                                                        <asp:DropDownList runat="server" AutoPostBack="true" 
                                                            ID="DropDownList_TabReportesDia_Invernaderos" OnSelectedIndexChanged="DropDownList_TabReportesDia_Invernaderos_OnSelectedIndexChanged"  DataValueField="ID_INVERNADERO" DataTextField="NOMBRE" 
                                                            CssClass="form-control" ondatabinding="DropDownList_TabReportesDia_Invernaderos_DataBinding"></asp:DropDownList>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <label runat="server"  id="lbltipo">Tipo</label>
                                                        <asp:DropDownList runat="server" AutoPostBack="true" 
                                                            ID="DropDown_TabReportesDia_ListTipoSensor" OnSelectedIndexChanged="DropDown_TabReportesDia_ListTipoSensor_OnSelectedIndexChanged" DataValueField="ID_TIPO_SENSOR" DataTextField="TIPO_SENSOR" 
                                                            CssClass="form-control" ondatabinding="DropDownList_TabReportesDia_TipoSensor_DataBinding"></asp:DropDownList>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Sensor</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup_TabReportesDia_Sensor" Theme="iOS" runat="server" 
                                                            placeholder="Sensor" KeyFieldName="ID_SENSOR" SelectionMode="Single" 
                                                            TextFormatString="{1}" Width="100%" ClientInstanceName="ClientGridLookup" 
                                                            ondatabinding="ASPxGridLookup_TabReportesDia_Sensor_DataBinding">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID_SENSOR" Width="30%" />
                                                                <dx:GridViewDataTextColumn Caption="Alias" FieldName="ALIAS" Width="30%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Fecha</label>
                                                        <dx:ASPxDateEdit ID="ASPxDateEdit_TabReportesDia_Fecha" runat="server" DisplayFormatString="dd/MM/yyyy" EditFormat="Custom" Width="100%" Theme="iOS">
                                                            
                                                        </dx:ASPxDateEdit>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-2" runat="server" visible="true">
                                                        <asp:Button ID="ButtonMostrarGraficaTodos" runat="server" CssClass="btn btn-info" 
                                                        Text="Cargar todos los sensores" Style="display: inline-block; margin-top: 20px;" 
                                                        onclick="ButtonMostrarGraficaTodos_Click" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <asp:Button ID="ButtonMostrarGrafica" runat="server" CssClass="btn btn-info" 
                                                        Text="Cargar Gráfica" Style="display: inline-block; margin-top: 20px;" 
                                                        onclick="ButtonMostrarGrafica_Click" />
                                                    </div>
                                                </div>

                                                <div class="row" style="margin-top: 0%;>
                                                    <div class="col-sm-12 col-md-12">
                                                        <asp:UpdatePanel runat="server" ID="UpdatePanelGraficaDatosDia" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <%--<div>
                                                                    <asp:Literal ID="lt" runat="server"></asp:Literal>
                                                                </div>   
                                                                <div id="chart_div"></div>--%>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto">       
                                                                                <asp:Chart ID="Chart1" runat="server" Width="1200px" Height="500px">
                                                                                    <Series>
                                                                                        
                                                                                     </Series>
                                                                                     <ChartAreas>
                                                                                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                                                                     </ChartAreas>
                                                                                 </asp:Chart>  
                                                                                 <asp:Chart ID="Chart2" runat="server" Width="1200px" Height="500px">
                                                                                    <Series>
                                                                                        
                                                                                     </Series>
                                                                                     <ChartAreas>
                                                                                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                                                                     </ChartAreas>
                                                                                 </asp:Chart>                         
                                                                                <%--<dx:WebChartControl OnCustomDrawSeriesPoint="GraficaDatosDia_CustomDrawSeriesPoint" ID="GraficaDatosDia" runat="server" Height="400px"
                                                                                Width="1000px" ClientInstanceName="chart"
                                                                                    CrosshairEnabled="False"  ToolTipEnabled="False"
                                                                                SeriesDataMember="ALIAS">
                                                                                    <SeriesTemplate ArgumentDataMember="HORA" ValueDataMembersSerializable="ACTUAL" LabelsVisibility="True"
                                                                                        CrosshairLabelPattern="HORA: {S}<br/>ACTUAL: {V}pz">
                                                                                        <ViewSerializable>
                                                                                             <dx:LineSeriesView Color="0, 0, 0">
                                                                                                 <linemarkeroptions size="13" color="0, 0, 0">
                                                                                                 </linemarkeroptions>
                                                                                             </dx:LineSeriesView>
                                                                                        </ViewSerializable>
                                                                                        <LabelSerializable>
                                                                                             <dx:PointSeriesLabel LineLength="18" TextPattern="{V}">
                                                                                             </dx:PointSeriesLabel>
                                                                                        </LabelSerializable>
                                                                                    </SeriesTemplate>
                                                                                 <Legend Direction="BottomToTop">
                                                                                </Legend>
                                                                                <BorderOptions Visibility="False" />
                                                                                <Titles>
                                                                                    <dx:ChartTitle Text="Reporte Datos por Día"></dx:ChartTitle>
                                                                                    <dx:ChartTitle Dock="Bottom" Alignment="Far" Text="" Font="Tahoma, 8pt" TextColor="Gray"></dx:ChartTitle>
                                                                                </Titles>
                                                                                <DiagramSerializable>
                                                                                    <dx:XYDiagram>
                                                                                        <AxisX Title-Text="Hora" VisibleInPanesSerializable="-1" interlaced="True" 
                                                                                            visibility="True" title-visibility="True">
                                                                                            <gridlines minorvisible="True" visible="True">
                                                                                            </gridlines>
                                                                                        </AxisX>
                                                                                        <AxisY Title-Text="Datos"  Title-Visibility="True" 
                                                                                            VisibleInPanesSerializable="-1">
                                                                                            <Label TextPattern="{V}"/>
                                                                                            <GridLines MinorVisible="True"></GridLines>
                                                                                        </AxisY>
                                                                                        <defaultpane>
                                                                                        </defaultpane>
                                                                                    </dx:XYDiagram>
                                                                                </DiagramSerializable>
                                                                            </dx:WebChartControl>--%>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                                                                        </asp:Panel>
                                                                    </div><!-- /.col-sm-8 -->
                                                                </div><!-- /.row -->
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


