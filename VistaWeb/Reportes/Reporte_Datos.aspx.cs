﻿using Core.Seguridad;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidades;
using System.Data;
using DevExpress.XtraCharts;


public partial class Reportes_Reporte_Datos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (UsuarioStatic.getObj(this) != null)
            {
                // La sesión se encuentra abierta
                if (Core.Seguridad.Accesos.TieneAcceso(UsuarioStatic.getId(this), SystemConstantes.WEB_FORM__REPORTE_DATOS.IdModulo))
                {
                    DateInicio.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    DateFin.Date = DateTime.Today;
                    Session["DATASET_REPORTE_DATOS"] = BLL.MulData.QueryLibre("SET DATEFORMAT DMY EXEC [OBTENER_REPORTE_DATOS_SENSOR] @inicio = '" + DateInicio.Date.ToShortDateString() + "' ,@fin = '" + DateFin.Date.ToShortDateString() + "' , @id_usuario = " + UsuarioStatic.getId(this));
                    this.ASPxGridView_DatosSensores.DataBind();
                    UpdatePanel_TabReportesDia.Update();
                }
                else
                {
                    // No tiene ningún acceso
                    this.Response.Redirect(SystemConstantes.WEB_FORM__SIN_ACCESO.UrlCompleta);
                }
            }
            else
            {
                // La sesión se encuentra cerrada
                this.Response.Redirect(SystemConstantes.WEB_FORM__LOGIN.UrlCompleta);
            }
        }
    }
    protected void ButtonActualizar_Click(object sender, EventArgs e)
    {
        Session["DATASET_REPORTE_DATOS"] = BLL.MulData.QueryLibre("SET DATEFORMAT DMY EXEC [OBTENER_REPORTE_DATOS_SENSOR] @inicio = '" + DateInicio.Date.ToShortDateString() + "' ,@fin = '" + DateFin.Date.ToShortDateString() + "' , @id_usuario = " + UsuarioStatic.getId(this));           
        ASPxGridView_DatosSensores.DataBind();
        UpdatePanel_TabReportesDia.Update();
    }
    protected void ASPxGridView_DatosSensores_OnDataBinding(object sender, EventArgs e)
    {
        ASPxGridView_DatosSensores.DataSource = Session["DATASET_REPORTE_DATOS"];
    }

    protected void LinkButton_TabReportesDia_Actualizar_OnClick(object sender, EventArgs e)
    {
        this.ASPxGridView_DatosSensores.DataBind();
        UpdatePanel_TabReportesDia.Update();
    }

}