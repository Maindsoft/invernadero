﻿using Core.Seguridad;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidades;
using System.Data;
using System.Text;
using System.Web.UI.DataVisualization.Charting;


public partial class Reportes_Reporte_Datos_Dia : System.Web.UI.Page
{
    StringBuilder str = new StringBuilder();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (UsuarioStatic.getObj(this) != null)
            {
                // La sesión se encuentra abierta
                if (Core.Seguridad.Accesos.TieneAcceso(UsuarioStatic.getId(this), SystemConstantes.WEB_FORM__REPORTE_DATOS_DIA.IdModulo))
                {
                    this.cargarPagina();
                    /*chart_bind();
                    UpdatePanelGraficaDatosDia.Update();*/
                }
                else
                {
                    // No tiene ningún acceso
                    this.Response.Redirect(SystemConstantes.WEB_FORM__SIN_ACCESO.UrlCompleta);
                }
            }
            else
            {
                // La sesión se encuentra cerrada
                this.Response.Redirect(SystemConstantes.WEB_FORM__LOGIN.UrlCompleta);
            }
        }
    }

    public void cargarPagina()
    {
        Session["hora"] = string.Empty;
        
        DropDownList_TabReportesDia_Invernaderos.DataBind();
        DropDown_TabReportesDia_ListTipoSensor.DataBind();
        ASPxGridLookup_TabReportesDia_Sensor.DataBind();
        ASPxDateEdit_TabReportesDia_Fecha.Date = DateTime.Now;
        UpdatePanel_TabReportesDia.Update();
        
    }

    protected void LinkButton_TabReportesDia_Actualizar_OnClick(object sender, EventArgs e)
    {
        this.cargarPagina();
    }

    protected void DropDownList_TabReportesDia_Invernaderos_DataBinding(object sender, EventArgs e)
    {
        this.DropDownList_TabReportesDia_Invernaderos.DataSource = Core.Invernaderos.GestionInvernaderos.Recuperar(UsuarioStatic.getId(this));
    }

    protected void DropDownList_TabReportesDia_Invernaderos_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //DropDown_TabReportesDia_ListTipoSensor.Visible = true;
        DropDown_TabReportesDia_ListTipoSensor.DataBind();
        ASPxGridLookup_TabReportesDia_Sensor.DataBind();
        UpdatePanel_TabReportesDia.Update();
    }


    protected void DropDownList_TabReportesDia_TipoSensor_DataBinding(object sender, EventArgs e)
    {
        int idInvernadero = string.IsNullOrEmpty(DropDownList_TabReportesDia_Invernaderos.SelectedValue) ? 0 : Convert.ToInt32(DropDownList_TabReportesDia_Invernaderos.SelectedValue);
        if (idInvernadero != 0)
        {
            DropDown_TabReportesDia_ListTipoSensor.DataSource = Core.Sensores.GestionSensores.ObtenerTiposSensoresPorInvernadero(idInvernadero, UsuarioStatic.getId(this));
        }
    }

    protected void DropDown_TabReportesDia_ListTipoSensor_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //DropDown_TabReportesDia_ListTipoSensor.Visible = true;
        ASPxGridLookup_TabReportesDia_Sensor.DataBind();
        UpdatePanel_TabReportesDia.Update();
    }


    protected void ASPxGridLookup_TabReportesDia_Sensor_DataBinding(object sender, EventArgs e)
    {
        int idInvernadero = string.IsNullOrEmpty(DropDownList_TabReportesDia_Invernaderos.SelectedValue) ? 0 : Convert.ToInt32(DropDownList_TabReportesDia_Invernaderos.SelectedValue);
        int idTipoSensor = string.IsNullOrEmpty(DropDown_TabReportesDia_ListTipoSensor.SelectedValue) ? 0 : Convert.ToInt32(DropDown_TabReportesDia_ListTipoSensor.SelectedValue);
        ASPxGridLookup_TabReportesDia_Sensor.DataSource = Core.Sensores.GestionSensores.obtenerSensoresPorTipo(idInvernadero, idTipoSensor, UsuarioStatic.getId(this),1);
    }

    protected void ButtonMostrarGrafica_Click(object sender, EventArgs e)
    {
        Chart1.Visible = false;

        if (string.IsNullOrEmpty(DropDownList_TabReportesDia_Invernaderos.SelectedValue)) {
            Notificaciones.showNotifyInElement(DropDownList_TabReportesDia_Invernaderos, "Seleccione un invernadero", Notificaciones.TIPO_ERROR, this);
        }
        else if (string.IsNullOrEmpty(DropDown_TabReportesDia_ListTipoSensor.SelectedValue))
        {
            Notificaciones.showNotifyInElement(DropDown_TabReportesDia_ListTipoSensor, "Seleccione un tipo de sensor", Notificaciones.TIPO_ERROR, this);
        }
        else if (ASPxGridLookup_TabReportesDia_Sensor.Value == null)
        {
            Notificaciones.showNotifyInElement(ASPxGridLookup_TabReportesDia_Sensor, "Seleccione un sensor", Notificaciones.TIPO_ERROR, this);
        }
        else if (string.IsNullOrEmpty(ASPxDateEdit_TabReportesDia_Fecha.Text))
        {
            Notificaciones.showNotifyInElement(ASPxDateEdit_TabReportesDia_Fecha, "Seleccione una fecha", Notificaciones.TIPO_ERROR, this);
        }
        else {
            int idInvernadero = Convert.ToInt32(DropDownList_TabReportesDia_Invernaderos.SelectedValue);
            int idTipoSensor = Convert.ToInt32(DropDown_TabReportesDia_ListTipoSensor.SelectedValue);
            int idSensor = Convert.ToInt32(ASPxGridLookup_TabReportesDia_Sensor.Value);
            string fecha = ASPxDateEdit_TabReportesDia_Fecha.Date.ToString("MM-dd-yyyy");
            chart_bind2(idSensor, idTipoSensor,fecha);
            //llenarDatosGrafica(idInvernadero, idSensor, idTipoSensor, fecha);

        }
    }

    protected void ButtonMostrarGraficaTodos_Click(object sender, EventArgs e)
    {
        Chart2.Visible = false;
        if (string.IsNullOrEmpty(DropDownList_TabReportesDia_Invernaderos.SelectedValue))
        {
            Notificaciones.showNotifyInElement(DropDownList_TabReportesDia_Invernaderos, "Seleccione un invernadero", Notificaciones.TIPO_ERROR, this);
        }
        else if (string.IsNullOrEmpty(DropDown_TabReportesDia_ListTipoSensor.SelectedValue))
        {
            Notificaciones.showNotifyInElement(DropDown_TabReportesDia_ListTipoSensor, "Seleccione un tipo de sensor", Notificaciones.TIPO_ERROR, this);
        }
        else if (string.IsNullOrEmpty(ASPxDateEdit_TabReportesDia_Fecha.Text))
        {
            Notificaciones.showNotifyInElement(ASPxDateEdit_TabReportesDia_Fecha, "Seleccione una fecha", Notificaciones.TIPO_ERROR, this);
        }
        else {
            int idInvernadero = Convert.ToInt32(DropDownList_TabReportesDia_Invernaderos.SelectedValue);
            int idTipoSensor = Convert.ToInt32(DropDown_TabReportesDia_ListTipoSensor.SelectedValue);
            string fecha = ASPxDateEdit_TabReportesDia_Fecha.Date.ToString("yyyy-MM-dd");
            chart_bind(idInvernadero, idTipoSensor, fecha);
            UpdatePanelGraficaDatosDia.Update();
            //llenarDatosGrafica(idInvernadero, 0, idTipoSensor, fecha);
        }
    }

    public void llenarDatosGrafica(int idInvernadero ,int idSensor, int idTipoSensor, string fecha)
    {
        //if (idInvernadero == 0)
        //{
            //GraficaDatosDia.DataSource = Core.Sensores.GestionSensores.obtenerGraficaRangosPorSensor(idSensor, idTipoSensor, UsuarioStatic.getId(this), fecha);

        //}
        //else {
            
        //    GraficaDatosDia.DataSource = Core.Sensores.GestionSensores.obtenerGraficaRangosPorInvernadero(idInvernadero, idTipoSensor, UsuarioStatic.getId(this), fecha);
            
        //}

        //GraficaDatosDia.DataBind();
        //UpdatePanelGraficaDatosDia.Update();
    }

    //public void GraficaDatosDia_CustomDrawSeriesPoint(object sender, CustomDrawSeriesPointEventArgs e)
    //{
    //    double dato = e.SeriesPoint.Values[0];
    //    string d = e.Series.Name;

    //    int idTipoSensor = Convert.ToInt32(DropDown_TabReportesDia_ListTipoSensor.SelectedValue);
        

    //    if (string.IsNullOrEmpty(Session["hora"].ToString()))
    //    {
    //        Session["hora"] = 0;
    //    }
    //    else {
    //        Session["hora"] = Convert.ToInt32(Session["hora"]) + 1;
    //    }


    //    //int idSensor = Convert.ToInt32(BLL.MulData.SelectCondicion("ID_SENSOR FROM SENSORES WHERE ALIAS = '" + d + "' AND ACTIVO_INACTIVO = 1").Rows[0]["ID_SENSOR"]);
    //    //e.SeriesPoint.Color = System.Drawing.ColorTranslator.FromHtml(Core.Sensores.GestionSensores.obtenerColoresGraficaRangosPorSensor(idSensor, idTipoSensor, UsuarioStatic.getId(this), dato, Convert.ToInt32(Session["hora"])));
        

    //    if (Session["hora"].ToString() == "23") { 
    //        Session["hora"] = string.Empty;
    //    }
    //}

    private void chart_bind(int idInvernadero, int idTipoSensor, string fecha)
    {
        Chart1.Visible = true;
        Chart2.Visible = false;
        UpdatePanelGraficaDatosDia.Update();
        DataTable dt = new DataTable();
        DataTable columns = new DataTable();
        try
        {
            columns = BLL.MulData.SelectCondicion(" '''' +ALIAS +'''' AS ALIAS, ALIAS AS ALIAS2  " +
                                                  " FROM DATOS_SENSORES DS "+
                                                  " JOIN SENSORES S ON DS.ID_SENSOR = S.ID_SENSOR "+
                                                  " WHERE ID_INVERNADERO =  "+ idInvernadero.ToString() +
                                                  " AND ID_TIPO_SENSOR =  "+ idTipoSensor.ToString() +
                                                  " GROUP BY ALIAS " +
                                                  " ORDER BY ALIAS ASC");

            dt = Core.Sensores.GestionSensores.obtenerGraficaRangosPorInvernadero(idInvernadero, idTipoSensor, UsuarioStatic.getId(this), fecha);

             
            Int32 i;
            int contador = 0;
            Chart1.Legends.Add("y");
            Chart1.ChartAreas[0].AxisX.MajorGrid.Interval = 1;
            Chart1.ChartAreas[0].AxisX.Interval = 1; //muestra cada cuando se plotea el date time en el
            Chart1.ChartAreas[0].AxisX.Minimum =0 ;
            Chart1.ChartAreas[0].AxisX.Maximum = 24;
            Chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            Chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            

            foreach (DataRow col in columns.Rows)
            {
                Chart1.Series.Add(col["ALIAS2"].ToString());
                Chart1.Series[contador].ChartType = SeriesChartType.Line;
                Chart1.Series[contador].MarkerStyle = MarkerStyle.Circle;
                Chart1.Series[contador].BorderWidth=2;
                contador++;
            }
            int contador2 = 0;
            for (i = 0; i <= dt.Rows.Count - 1; i++)
            {
                string hora = dt.Rows[i]["HORA"].ToString();
                foreach (DataRow col in columns.Rows)
                {
                    string value = string.Empty;
                    value = dt.Rows[i][col["ALIAS2"].ToString()] is DBNull ? "0" : dt.Rows[i][col["ALIAS2"].ToString()].ToString();
                    value = value.Replace(",", ".");
                    DataPoint point = new DataPoint();
                    point.SetValueXY(hora, value);
                    point.ToolTip = string.Format(col["ALIAS2"].ToString() + " Hora: {0}:00, Valor: {1}", hora, value);
                    point.LabelToolTip = string.Format("{0}, {1}", hora, value);
                    point.LegendToolTip = string.Format("{0}, {1}", hora, value);
                    Chart1.Series[col["ALIAS2"].ToString()].Points.Add(point);

                    contador2++;
                }
               
            }
           
            
        }
        catch
        { }
    }

    private void chart_bind2(int idSensor, int idTipoSensor, string fecha)
    {
        Chart1.Visible = false;
        Chart2.Visible = true;
        UpdatePanelGraficaDatosDia.Update();
        
        DataTable dt = new DataTable();
        DataTable columns = new DataTable();
        DataTable rangos = new DataTable();
        try
        {
            columns = BLL.MulData.SelectCondicion(" '''' +ALIAS +'''' AS ALIAS, ALIAS AS ALIAS2  " +
                                                  " FROM DATOS_SENSORES DS " +
                                                  " JOIN SENSORES S ON DS.ID_SENSOR = S.ID_SENSOR " +
                                                  " WHERE S.ID_SENSOR =  " + idSensor.ToString() +
                                                  " AND ID_TIPO_SENSOR =  " + idTipoSensor.ToString() +
                                                  " GROUP BY ALIAS " +
                                                  " ORDER BY ALIAS ASC");

            rangos = BLL.MulData.SelectCondicion(" DATEPART(HOUR, HORA) as HORA, "+
                                                 " isnull(RANGO_IDEAL_INFERIOR_ROJO,0) as RANGO_IDEAL_INFERIOR_ROJO, " +
                                                 " isnull(RANGO_IDEAL_INFERIOR_AMARILLO,0) as RANGO_IDEAL_INFERIOR_AMARILLO, " +
                                                 " isnull(RANGO_IDEAL_INFERIOR_VERDE,0) as RANGO_IDEAL_INFERIOR_VERDE, " +
                                                 " isnull(RANGO_IDEAL_SUPERIOR_VERDE,0) as RANGO_IDEAL_SUPERIOR_VERDE, " +
                                                 " isnull(RANGO_IDEAL_SUPERIOR_AMARILLO,0) as RANGO_IDEAL_SUPERIOR_AMARILLO, " +
                                                 " isnull(RANGO_IDEAL_SUPERIOR_ROJO,0) as RANGO_IDEAL_SUPERIOR_ROJO" +
                                                 " from RANGOS_SENSORES "+
                                                 " where ID_SENSOR = " + idSensor.ToString() +
                                                 " and ID_TIPO_SENSOR = " + idTipoSensor.ToString());

            dt = BLL.MulData.QueryLibre("EXEC [OBTENER_GRAFICA_RANGOS_POR_SENSOR2] @idSensor = " + idSensor.ToString() + ", @idTipoSensor = " + idTipoSensor + ", @fecha = '" + fecha + "', @idUsuario = " + UsuarioStatic.getId(this));
            

            Int32 i;
            int contador = 0;
            Chart2.Legends.Add("y");
            Chart2.ChartAreas[0].AxisX.MajorGrid.Interval = 1;
            Chart2.ChartAreas[0].AxisX.Interval = 1; //muestra cada cuando se plotea el date time en el
            Chart2.ChartAreas[0].AxisX.Minimum = 0; 
            Chart2.ChartAreas[0].AxisX.Maximum = 24;
            Chart2.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            Chart2.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;


            foreach (DataRow col in columns.Rows)
            {
                Chart2.Series.Add(col["ALIAS2"].ToString());
                Chart2.Series[contador].ChartType = SeriesChartType.Line;
                Chart2.Series[contador].MarkerStyle = MarkerStyle.Circle;
                Chart2.Series[contador].BorderWidth = 2;
                contador++;
            }
            int contador2 = 0;
            for (i = 0; i <= dt.Rows.Count - 1; i++)
            {
                string hora = dt.Rows[i]["HORA"].ToString();
                DataRow rangosRow = rangos.Rows[i];
                foreach (DataRow col in columns.Rows)
                {
                    string value = string.Empty;
                    double value2;
                    value = dt.Rows[i][col["ALIAS2"].ToString()] is DBNull ? "0" : dt.Rows[i][col["ALIAS2"].ToString()].ToString();
                    value2 = Convert.ToDouble(value);
                    value = value.Replace(",", ".");
                    DataPoint point = new DataPoint();
                    point.SetValueXY(hora, value);
                    point.ToolTip = string.Format(col["ALIAS2"].ToString() + " Hora: {0}:00, Valor: {1}", hora, value);
                    point.LabelToolTip = string.Format("{0}, {1}", hora, value);
                    point.LegendToolTip = string.Format("{0}, {1}", hora, value);
                    if (value2 < Convert.ToDouble(rangosRow["RANGO_IDEAL_INFERIOR_ROJO"])) {
                        point.BorderColor = System.Drawing.Color.Red;
                        point.Color = System.Drawing.Color.Red;
                    }
                    else if (value2 >= Convert.ToDouble(rangosRow["RANGO_IDEAL_INFERIOR_ROJO"]) && value2 < Convert.ToDouble(rangosRow["RANGO_IDEAL_INFERIOR_AMARILLO"]))
                    {
                        point.BorderColor = System.Drawing.Color.Red;
                        point.Color = System.Drawing.Color.Red;
                    }
                    else if (value2 >= Convert.ToDouble(rangosRow["RANGO_IDEAL_INFERIOR_AMARILLO"]) && value2 < Convert.ToDouble(rangosRow["RANGO_IDEAL_INFERIOR_VERDE"]))
                    {
                        point.BorderColor = System.Drawing.Color.Yellow;
                        point.Color = System.Drawing.Color.Yellow;
                    }
                    else if (value2 >= Convert.ToDouble(rangosRow["RANGO_IDEAL_INFERIOR_VERDE"]) && value2 < Convert.ToDouble(rangosRow["RANGO_IDEAL_SUPERIOR_VERDE"]))
                    {
                        point.BorderColor = System.Drawing.Color.Green;
                        point.Color = System.Drawing.Color.Green;
                    }
                    else if (value2 >= Convert.ToDouble(rangosRow["RANGO_IDEAL_SUPERIOR_VERDE"]) && value2 < Convert.ToDouble(rangosRow["RANGO_IDEAL_SUPERIOR_AMARILLO"]))
                    {
                        point.BorderColor = System.Drawing.Color.Green;
                        point.Color = System.Drawing.Color.Green;
                    }
                    else if (value2 >= Convert.ToDouble(rangosRow["RANGO_IDEAL_SUPERIOR_AMARILLO"]) && value2 < Convert.ToDouble(rangosRow["RANGO_IDEAL_SUPERIOR_ROJO"]))
                    {
                        point.BorderColor = System.Drawing.Color.Yellow;
                        point.Color = System.Drawing.Color.Yellow;
                    }
                    else if (value2 >= Convert.ToDouble(rangosRow["RANGO_IDEAL_SUPERIOR_ROJO"]))
                    {
                        point.BorderColor = System.Drawing.Color.Red;
                        point.Color = System.Drawing.Color.Red;
                    }

                    point.MarkerSize = 10;

                    Chart2.Series[col["ALIAS2"].ToString()].Points.Add(point);

                    contador2++;
                }

            }


        }
        catch
        { }
    }

}