﻿using Core.Seguridad;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidades;
using System.Data;
using DevExpress.XtraPrinting;

public partial class Usuarios : System.Web.UI.Page
{
    
    private const Int32 MODULO_PRIMER_NIVEL = 1;
   
    private const Int32 MODULO_SEGUNDO_NIVEL = 2;
    private const Int32 MODULO_ID = 2;
    private const string SESSION_VAR__THIS_DATASET = "DATASET_Usuarios.aspx";
    private const string Modal_EditarUsuario_ = "Modal_EditarUsuario";

    // Evento que se ejecuta cuando se va lanza el formulario por primera vez
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (UsuarioStatic.getObj(this) != null)
            {
                // La sesión se encuentra abierta
                if (Core.Seguridad.Accesos.TieneAcceso(UsuarioStatic.getId(this), MODULO_ID))
                {
                    this.Page_InDataCharge();

                    // Para todos los accesos
                   
                    this.ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto.DataBind();
                    this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos.DataBind();


                    this.UpdatePanel_TabCapturaUsuarios.Update();
                }
                else
                { 
                    // No tiene ningún acceso
                    this.Response.Redirect(SystemConstantes.WEB_FORM__SIN_ACCESO.UrlCompleta);
                }
            }
            else
            { 
                // La sesión se encuentra cerrada
                this.Response.Redirect(SystemConstantes.WEB_FORM__LOGIN.UrlCompleta);
            }
        }
    }

    // Método que se ejecuta cuando se recarga toda la información inicial del formulario
    protected void Page_InDataCharge()
    {
        if (!IsPostBack)
        {
            this.Session[SESSION_VAR__THIS_DATASET] = null;
            ParentDataSet data = new ParentDataSet();
            // START ####

            data.agregarKeyValueDataQuery(this.ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto_BuildQuery());
            data.agregarKeyValueDataQuery(this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos_BuildQuery());

            // END ######
            data.EjecutarConsultas();
            this.Session[SESSION_VAR__THIS_DATASET] = data;
        }
    }

    /*
            Tab Captura de Usuarios
     */


    

    protected void ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto_OnDataBinding(object sender, EventArgs e)
    {
        this.ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto.DataSource = ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).obtenerDataUnit(this.ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto.ID).Copy();
    }

    protected KeyValueDataQuery ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto_BuildQuery()
    {
        string query = string.Empty;
        string key = this.ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto.ID;

        query =
        "ID_PERFIL_PUESTO AS ID, " +
        "NOMBRE_PERFIL AS DESCRIPCION " +
        "FROM PERFILES_PUESTO WHERE ACTIVO_INACTIVO = 1 ";

        return new KeyValueDataQuery(key, query);
    }

    protected void LinkButton_TabCapturaUsuarios_DescargarExcel_OnClick(object sender, EventArgs e)
    {
        XlsxExportOptionsEx conf = new XlsxExportOptionsEx();
        conf.ExportType = DevExpress.Export.ExportType.DataAware;
        string name = "Usuarios activos - " + DateTime.Now.ToString().Replace("-", "_").Replace(".", "_");
        this.ASPxGridViewExporter_ASPxGridView_TabCapturaUsuarios_UsuariosActivos.WriteXlsxToResponse(name, conf);
    }

    protected void LinkButton_TabCapturaUsuarios_Actualizar_OnClick(object sender, EventArgs e)
    {
        ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).refrescarDataUnit(ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto.ID);
        this.ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto.DataBind();

        ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).refrescarDataUnit(ASPxGridView_TabCapturaUsuarios_UsuariosActivos.ID);
        this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos.DataBind();
        this.UpdatePanel_TabCapturaUsuarios.Update();
    }

    protected void Button_TabCapturaUsuarios_Guardar_OnClick(object sender, EventArgs e)
    {
        string nombre = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_TabCapturaUsuarios_Nombre.Text);
        if (string.IsNullOrWhiteSpace(nombre))
        {
            Notificaciones.showNotify("Ingrese el nombre del usuario", Notificaciones.TIPO_ERROR, this);
            return;
        }

        string apellido_paterno = this.TextBox_TabCapturaUsuarios_ApellidoPaterno.Text;
        /*if (string.IsNullOrWhiteSpace(apellido_paterno))
        {
            Notificaciones.showNotify("Ingrese el apellido paterno", Notificaciones.TIPO_ERROR, this);
            return;
        }*/

        string apellido_materno = this.TextBox_TabCapturaUsuarios_ApellidoMaterno.Text;
        /*if (string.IsNullOrWhiteSpace(apellido_materno))
        {
            Notificaciones.showNotify("Ingrese el apellido materno", Notificaciones.TIPO_ERROR, this);
            return;
        }*/

      
        

        if (string.IsNullOrWhiteSpace(Convert.ToString(this.ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto.Value)))
        {
            Notificaciones.showNotify("Seleccione un perfil de puesto", Notificaciones.TIPO_ERROR, this);
            return;
        }

        string correo_electronico = this.TextBox_TabCapturaUsuarios_CorreoElectronico.Text;
        if (string.IsNullOrWhiteSpace(correo_electronico))
        {
            Notificaciones.showNotify("Ingrese el correo electrónico", Notificaciones.TIPO_ERROR, this);
            return;
        }
        /*if (!Validador.ValidarEmail(correo_electronico))
        {
            Notificaciones.showNotify("Ingrese un correo electrónico válido", Notificaciones.TIPO_ERROR, this);
            return;
        }*/
        if (UsuarioStatic.existeEmail(correo_electronico))
        {
            Notificaciones.showNotify("La dirección de correo electrónico ya existe", Notificaciones.TIPO_ERROR, this);
            return;
        }

        string pass = this.TextBox_TabCapturaUsuarios_Password.Text;
        if (string.IsNullOrWhiteSpace(pass))
        {
            Notificaciones.showNotify("Ingrese la contraseña", Notificaciones.TIPO_ERROR, this);
            return;
        }

        Core.Usuarios.GestionUsuarios.Crear(
            nombre,
            correo_electronico.ToLower(),
            pass,
            Convert.ToInt32(this.ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto.Value),
            apellido_paterno,
            apellido_materno,
            UsuarioStatic.getId(this)

        );

        Notificaciones.showNotify("Se ha creado el usuario de manera exitosa", Notificaciones.TIPO_SUCCESS, this);

        this.TextBox_TabCapturaUsuarios_Nombre.Text = string.Empty;
        this.TextBox_TabCapturaUsuarios_ApellidoPaterno.Text = string.Empty;
        this.TextBox_TabCapturaUsuarios_ApellidoMaterno.Text = string.Empty;
  
        this.ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto.Value = null;
        this.TextBox_TabCapturaUsuarios_CorreoElectronico.Text = string.Empty;
        this.TextBox_TabCapturaUsuarios_Password.Text = string.Empty;
        ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).refrescarDataUnit(this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos.ID);
        this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos.DataBind();

        this.UpdatePanel_TabCapturaUsuarios.Update();
    }

    protected void ASPxGridView_TabCapturaUsuarios_UsuariosActivos_OnRowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        Int32 ID = Convert.ToInt32(ASPxGridView_TabCapturaUsuarios_UsuariosActivos.GetRowValues(e.VisibleIndex, "ID"));
        switch (e.CommandArgs.CommandName)
        {
            case "EDITAR":
                this.Modal_EditarUsuario_InDataCharge(ID);

                break;
            case "ELIMINAR":
                Core.Usuarios.GestionUsuarios.Eliminar(ID, UsuarioStatic.getId(this));
                ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).refrescarDataUnit(this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos.ID);
                this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos.DataBind();
               
                this.UpdatePanel_TabCapturaUsuarios.Update();
                Notificaciones.showNotify("Se ha eliminado con éxito", Notificaciones.TIPO_SUCCESS, this);
                break;
            default:

                break;
        }
    }

    protected void ASPxGridView_TabCapturaUsuarios_UsuariosActivos_OnDataBinding(object sender, EventArgs e)
    {
        this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos.DataSource = ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).obtenerDataUnit(this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos.ID).Copy();
    }

    protected KeyValueDataQuery ASPxGridView_TabCapturaUsuarios_UsuariosActivos_BuildQuery()
    {
        string query = string.Empty;
        string key = this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos.ID;

        if (Core.Seguridad.Accesos.RecuperarAcceso(UsuarioStatic.getId(this), MODULO_ID) == 2)
        {
            query =
            "ID_USUARIO AS ID, " +
            "ISNULL(U.NOMBRE, '') + ' ' + ISNULL(APELLIDO_PATERNO, '') + ' ' + ISNULL(APELLIDO_MATERNO, '') AS NOMBRE, " +
            "USUARIO, " +
            "PP.NOMBRE_PERFIL " +
            "FROM USUARIOS U "+
            "JOIN PERFILES_PUESTO PP ON " +
            "U.ID_PERFIL_PUESTO = PP.ID_PERFIL_PUESTO " +
            "WHERE U.ACTIVO_INACTIVO = 1 ";
        }
        else
        {
            query =
            "ID_USUARIO AS ID, " +
            "ISNULL(U.NOMBRE, '') + ' ' + ISNULL(APELLIDO_PATERNO, '') + ' ' + ISNULL(APELLIDO_MATERNO, '') AS NOMBRE, " +
            "USUARIO, " +
            "PP.NOMBRE_PERFIL " +
            "FROM USUARIOS U " +
            "JOIN PERFILES_PUESTO PP ON " +
            "U.ID_PERFIL_PUESTO = PP.ID_PERFIL_PUESTO " +
            "WHERE U.ACTIVO_INACTIVO = 1 ";
        }

        return new KeyValueDataQuery(key, query);
    }
    

    /*
            Modal para editar usuario
     */

    // Método que recarga la información completa del modal
    protected void Modal_EditarUsuario_InDataCharge(int idUsuario)
    {
        Core.Objetos.Usuario data = Core.Usuarios.GestionUsuarios.Recuperar(idUsuario, UsuarioStatic.getId(this));
        // Coloca el ID del usuario a editar
        this.HiddenField_ModalEditarUsuario_IdUsuario.Value = idUsuario.ToString();

        // Coloca el usuario
        this.TextBox_Modal_EditarUsuario_Nombre.Text = data.nombre;
        // Coloca el apellido paterno
        this.TextBox_Modal_EditarUsuario_ApellidoPaterno.Text = data.apellido_paterno;
        // Coloca el apellido materno
        this.TextBox_Modal_EditarUsuario_ApellidoMaterno.Text = data.apellido_materno;
        
        // Coloca el ID del perfil de puesto
        this.ASPxGridLookup_Modal_EditarUsuario_PerfilPuesto.DataBind();
        this.ASPxGridLookup_Modal_EditarUsuario_PerfilPuesto.Value = data.id_perfil_puesto;
        // Coloca el correo electrónico
        this.TextBox_Modal_EditarUsuario_CorreoElectronico.Text = data.correo_electronico;
                
        this.UpdatePanel_ModalEditarUsuario.Update();
        Notificaciones.ShowPopupDevExpress(Modal_EditarUsuario_, this);
    }

    // Evento que recarga la infromación de los perfiles de puesto
    protected void ASPxGridLookup_Modal_EditarUsuario_PerfilPuesto_OnDataBinding(object sender, EventArgs e)
    {
        this.ASPxGridLookup_Modal_EditarUsuario_PerfilPuesto.DataSource = ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).obtenerDataUnit(this.ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto.ID).Copy();
    }

    // Botón que guarda la edición del usuario
    protected void Button_ModalEditarUsuario_GuardarCambios_OnClick(object sender, EventArgs e)
    {
        int idUsuario = Convert.ToInt32(this.HiddenField_ModalEditarUsuario_IdUsuario.Value);
        string query = string.Empty;

        // Valida el nombre
        string nombre = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_Modal_EditarUsuario_Nombre.Text);
        if (string.IsNullOrWhiteSpace(nombre))
        {
            Notificaciones.showNotify("Ingrese un nombre", Notificaciones.TIPO_ERROR, this);
            return;
        }

        // Valida el apellido paterno
        string apellido_paterno = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_Modal_EditarUsuario_ApellidoPaterno.Text);
        /*if (string.IsNullOrWhiteSpace(apellido_paterno))
        {
            Notificaciones.showNotify("Ingrese el apellido paterno", Notificaciones.TIPO_ERROR, this);
            return;
        }*/

        // Valida el apellido materno
        string apellido_materno = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_Modal_EditarUsuario_ApellidoMaterno.Text);
        /*if (string.IsNullOrWhiteSpace(apellido_materno))
        {
            Notificaciones.showNotify("Ingrese el apellido materno", Notificaciones.TIPO_ERROR, this);
            return;
        }*/


        // Valida el perfil de puesto
        if (string.IsNullOrWhiteSpace(Convert.ToString(this.ASPxGridLookup_Modal_EditarUsuario_PerfilPuesto.Value)))
        {
            Notificaciones.showNotify("Seleccione un perfil de puesto", Notificaciones.TIPO_ERROR, this);
            return;
        }
        int id_perfil_puesto = Convert.ToInt32(this.ASPxGridLookup_Modal_EditarUsuario_PerfilPuesto.Value);

        // Valida el correo electrónico
        string correo_electronico = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_Modal_EditarUsuario_CorreoElectronico.Text);
        if (string.IsNullOrWhiteSpace(correo_electronico))
        {
            Notificaciones.showNotify("Ingrese el correo electrónico", Notificaciones.TIPO_ERROR, this);
            return;
        }
       /* if (!Validador.ValidarEmail(correo_electronico))
        {
            Notificaciones.showNotify("Ingrese un correo electrónico válido", Notificaciones.TIPO_ERROR, this);
            return;
        }*/
        // Verifica si el email ha cambiado
        if (correo_electronico != UsuarioStatic.getEmail(idUsuario))
        {
            if (UsuarioStatic.existeEmail(correo_electronico))
            {
                Notificaciones.showNotify("La dirección de correo electrónico ya existe", Notificaciones.TIPO_ERROR, this);
                return;
            }
        }
        

        // Verifica si se va a editar la contraseña
        bool editar_password = false;
        string password = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_Modal_EditarUsuario_Password.Text);
        if (!string.IsNullOrWhiteSpace(password))
        {
            editar_password = true;
        }




        // Actualiza los datos generales
        Core.Usuarios.GestionUsuarios.Actualizar(nombre, correo_electronico.ToLower(), id_perfil_puesto, apellido_paterno, apellido_materno, idUsuario, UsuarioStatic.getId(this));
       

        // Verifica si se va a actualizar la contraseña
        if (editar_password)
        {
            Core.Usuarios.GestionUsuarios.Actualizar_Contraseña(password, idUsuario, UsuarioStatic.getId(this));
        
        }

        ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).refrescarDataUnit(this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos.ID);
        this.ASPxGridView_TabCapturaUsuarios_UsuariosActivos.DataBind();
        this.UpdatePanel_TabCapturaUsuarios.Update();
        Notificaciones.HidePopupDevExpress(Modal_EditarUsuario_, this);
        Notificaciones.showNotify("Se ha editado con éxito", Notificaciones.TIPO_SUCCESS, this);
    }

    // Botón que permite cancelar la edición del usuario
    protected void Button_ModalEditarUsuario_CerrarModal_OnClick(object sender, EventArgs e)
    {
        Notificaciones.HidePopupDevExpress(Modal_EditarUsuario_, this);
    }


}