﻿<%@ Page Title="Bienvenido" Language="C#" MasterPageFile="~/Layout.master" AutoEventWireup="true"
    CodeFile="Home.aspx.cs" Inherits="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_Principal" runat="Server">
    <style>
        a:link, a:visited, a:hover, a:active
        {
            text-decoration: none;
        }       
        
        h3
        {
            color: Black;
        }
    </style>
    <div class="container-fluid">
        <div class="col g-ml-45 g-ml-0--lg g-pb-65--md">
            <div class=" g-pa-20">

                <div class="row">
                    <asp:Repeater runat="server" ID="Repeater_Imagenes_Menu" OnDataBinding="Repeater_Imagenes_Menu_OnDataBinding">
                        <ItemTemplate>
                            
                            <div class="col-sm-6 col-lg-6 col-xl-3 g-mb-30" style="text-align:center;">
                                <!-- Panel -->
                                    <div class="card-block g-font-weight-300 g-pa-20">
                                        
                                         <a id="A1" runat="server" href='<%# Eval("RUTA") %>'>
                                            <img id="Img1" runat="server" style="border: 0; width: 100%; max-width: 130px;" src='<%# Eval("RUTA_IMAGEN") %>'
                                                alt="Producción" />
                                            <h3>
                                                <%# Eval("NOMBRE_MODULO")%></h3>
                                        </a>

                                    </div>
                                <!-- End Panel -->
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
