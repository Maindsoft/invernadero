﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BLL;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[SystemConstantes.SESSION_VAR___ID_USUARIO] == null)
        {
            Session[SystemConstantes.SESSION_VAR__LAST_URL] = Request.Url.ToString();
            Response.Redirect("ingresar");

        }

        if (!IsPostBack)
        {
            Repeater_Imagenes_Menu.DataBind();
        }
    }

    protected void Repeater_Imagenes_Menu_OnDataBinding(object sender, EventArgs e)
    {
        Repeater_Imagenes_Menu.DataSource = BLL.MulData.SelectCondicion(" ID = CONVERT(VARCHAR,[ID_MODULO])+'_MODULO', [NOMBRE_MODULO], [ICONO], [RUTA], RUTA_IMAGEN = '~/Public/Images/'+RUTA+'.png' " +
            "FROM [MODULOS] M   " +
            "WHERE (VISIBLE = 1 OR ID_MODULO IN   " +
            "	(SELECT ID_MODULO   " +
            "	FROM PERFIL_PUESTO_SEGURIDAD   " +
            "	WHERE ID_PERFIL_PUESTO IN  " +
            "		(SELECT ID_PERFIL_PUESTO   " +
            "		FROM USUARIOS   " +
            "		WHERE ID_USUARIO = '" + Session[SystemConstantes.SESSION_VAR___ID_USUARIO] + "')))   " +
            "AND MODULO_PADRE IS NOT NULL AND ACTIVO_INACTIVO = 1  " +
            "ORDER BY [NOMBRE_MODULO] ");
    }
}