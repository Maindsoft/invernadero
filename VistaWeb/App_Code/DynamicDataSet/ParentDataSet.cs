﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for ParentDataSet
/// </summary>
public class ParentDataSet
{
    // Variable privada que guarda la referencia al objeto DataSet
    private DataSet _data_set;

    private List<KeyValueDataQuery> keyquery_collection;

    // Constructor
    public ParentDataSet()
    {
        // Inicializa el dataset
        _data_set = new DataSet();

        // Inicializa la lista de keyquerys
        this.keyquery_collection = new List<KeyValueDataQuery>();

    }

    public void EjecutarConsultas()
    {
        _data_set.Tables.Clear();

        foreach (KeyValueDataQuery keyquery in keyquery_collection)
        {
            DataTable data = BLL.MulData.SelectCondicion(keyquery.getValue());
            data.TableName = keyquery.getKey();
            _data_set.Tables.Add(data.Copy());
        }
    }

    public void agregarKeyValueDataQuery(KeyValueDataQuery keyquery)
    {
        keyquery_collection.Add(keyquery);
    }

    public DataTable obtenerDataUnit(string clave)
    {
        return (DataTable)_data_set.Tables[clave];
    }

    public bool removerDataUnit(string clave)
    {
        int index = _data_set.Tables.IndexOf(clave);
        if (index >= 0)
        {
            _data_set.Tables.RemoveAt(index);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool refrescarDataUnit(string clave)
    {
        int index = _data_set.Tables.IndexOf(clave);
        if (index >= 0)
        {
            _data_set.Tables.RemoveAt(index);

            DataTable data = BLL.MulData.SelectCondicion((this.keyquery_collection.Find(s => s.getKey() == clave).getValue()));
            data.TableName = clave;
            _data_set.Tables.Add(data.Copy());

            return true;
        }
        else
        {
            return false;
        }
    }

    public static ParentDataSet convertToParentDataSet(Object objeto)
    {
        return (ParentDataSet)objeto;
    }

}