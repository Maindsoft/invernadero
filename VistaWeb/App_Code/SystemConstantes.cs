﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

/// <summary>
/// Summary description for SystemConstantes
/// </summary>
public class SystemConstantes
{
    public const string CulturaActualString = "en-US";

    public static CultureInfo CulturaActual = new CultureInfo(CulturaActualString);

    public const string SESSION_VAR___ID_USUARIO = "SESSION_ID_USUARIO";

    public const string SESSION_VAR__LAST_URL = "SESSION_LAST_URL";

    public struct WEB_FORM__HOME
    {
        public static string Nombre = "Home";
        public static string Url = "inicio";
        public static string UrlCompleta = "~/" + Url;
        public static string RutaFisica = "~/Home.aspx";
        public static int IdModulo = 0;
    }

    //INVERNADEROS 
    public struct WEB_FORM__GESTION_INVERNADEROS
    {
        public static string Nombre = "Invernaderos";
        public static string Url = "gestion-invernaderos";
        public static string UrlCompleta = "~/" + Url;
        public static string RutaFisica = "~/Invernaderos/GestionInvernaderos.aspx";
        public static int IdModulo = 4;
    }

    public struct WEB_FORM__LOGIN
    {
        public static string Nombre = "Login";
        public static string Url = "ingresar";
        public static string UrlCompleta = "~/" + Url;
        public static string RutaFisica = "~/Login.aspx";
        public static int IdModulo = 0;
    }

    public struct WEB_FORM__SIN_ACCESO
    {
        public static string Nombre = "Sin Acceso";
        public static string Url = "sin-acceso";
        public static string UrlCompleta = "~/" + Url;
        public static string RutaFisica = "~/SinAcceso.aspx";
        public static int IdModulo = 0;
    }

    public struct WEB_FORM__GESTION_USUARIOS
    {
        public static string Nombre = "Módulo de gestión de usuarios";
        public static string Url = "gestion-usuarios";
        public static string UrlCompleta = "~/" + Url;  
        public static string RutaFisica = "~/Usuarios/Usuarios.aspx";
        public static int IdModulo = 2;
    }

    public struct WEB_FORM__GESTION_SENSORES
    {
        public static string Nombre = "Módulo de gestión de sensores";
        public static string Url = "gestion-sensores";
        public static string UrlCompleta = "~/" + Url;
        public static string RutaFisica = "~/Sensores/GestionSensores.aspx";
        public static int IdModulo = 6;
    }


    public struct WEB_FORM__PERFILES_INVERNADEROS
    {
        public static string Nombre = "Módulo para agregar perfiles de puesto a invernaderos";
        public static string Url = "perfiles-invernaderos";
        public static string UrlCompleta = "~/" + Url;
        public static string RutaFisica = "~/Usuarios/PerfilesInvernaderos.aspx";
        public static int IdModulo = 7;
    }

    public struct WEB_FORM__REPORTE_DATOS_DIA
    {
        public static string Nombre = "Módulo que obtiene el reporte de datos registrados por día";
        public static string Url = "reporte-datos-dia";
        public static string UrlCompleta = "~/" + Url;
        public static string RutaFisica = "~/Reportes/Reporte_Datos_Dia.aspx";
        public static int IdModulo = 9;
    }


    public struct WEB_FORM__REPORTE_DATOS
    {
        public static string Nombre = "Módulo que obtiene el reporte de datos registrados";
        public static string Url = "reporte-datos";
        public static string UrlCompleta = "~/" + Url;
        public static string RutaFisica = "~/Reportes/Reporte_Datos.aspx";
        public static int IdModulo = 10;
    }

}