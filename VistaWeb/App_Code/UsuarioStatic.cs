﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for UsuarioStatic
/// </summary>
public class UsuarioStatic
{
    public static object getObj(System.Web.UI.Page pagina)
    {
        return pagina.Session[SystemConstantes.SESSION_VAR___ID_USUARIO];
    }

    public static int getId(System.Web.UI.Page pagina)
    {
        return Convert.ToInt32(pagina.Session[SystemConstantes.SESSION_VAR___ID_USUARIO]);
    }

    public static int getIdEstablecimiento(System.Web.UI.Page pagina)
    {
        int idUsuario = Convert.ToInt32(pagina.Session[SystemConstantes.SESSION_VAR___ID_USUARIO]);
        DataTable data = BLL.MulData.SelectCondicion("ID_PERFIL_PUESTO FROM USUARIOS WHERE ID_USUARIO = '" + idUsuario.ToString() + "'");
        return Convert.ToInt32(data.Rows[0]["ID_PERFIL_PUESTO"]);
    }

    public static string getEmail(int idUsuario)
    {
        DataTable data = BLL.MulData.SelectCondicion("USUARIO FROM USUARIOS WHERE ID_USUARIO = '" + idUsuario.ToString() + "'");
        return Convert.ToString(data.Rows[0]["USUARIO"]);
    }

    public static bool existeEmail(string email)
    {
        DataTable data = BLL.MulData.SelectCondicion("ID_USUARIO FROM USUARIOS WHERE USUARIO = '" + email.ToLower() + "' AND ACTIVO_INACTIVO = 1 ");
        if (data.Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}