﻿<%@ page title="Estudios" language="C#" masterpagefile="~/Layout.master" autoeventwireup="true" inherits="Estudios_Estudios, App_Web_3bgualqq" %>

<asp:Content ID="Content_Principal" ContentPlaceHolderID="ContentPlaceHolder_Principal"
    runat="Server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            var gv = s.GetGridView();
            if (gv.cpValueChanged2) {

                document.getElementById('<%=ButtonllenarTipoProcedencia.ClientID%>').click();
            }
            if (gv.cpValueChanged3) {

                document.getElementById('<%=ButtonllenarEdad.ClientID%>').click();
            }

        }
    </script>
    <!-- CONTAINER START -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <!-- NAV START -->
                <ul class="nav u-nav-v5-1" role="tablist" data-target="nav-5-1-default-hor-left-icons"
                    data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-lightgray">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#TabCapturaUsuarios"
                        role="tab" onclick="CambiarTabCapturaUsuarios()"><i class="et-icon-clipboard u-tab-line-icon-pro g-mr-3">
                        </i>Captura Estudio </a></li>
                </ul>
                <!-- NAV END -->
                <!-- TAB PANES START -->
                <div id="nav-5-1-default-hor-left-icons" class="tab-content g-pt-20">
                    <!-- PESTAÑA CAPTURA DE ESTUDIOS START -->
                    <div class="tab-pane fade show active" id="TabCapturaUsuarios" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanel_TabCapturaUsuarios" runat="server" ChildrenAsTriggers="false"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Captura de Estudio</h3>
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">
                                            <div runat="server" id="LabelRecuperaEstudio" class="alert alert-warning" role="alert" visible="false">
                                                Se recuperó el estudio que estaba en proceso de captura :)
                                            </div>
                                            <div runat="server" id="LabelNoEditarPrecios" class="alert alert-warning" role="alert" visible="false">
                                                No se pueden editar los precios por que el estudio ya tiene cobros registrados
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Clave</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaEstudio_ID" runat="server" CssClass="form-control" PlaceHolder="Clave Estudio" Enabled="false"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaEstudio_ID" runat="server" TargetControlID="TextBox_TabCapturaEstudio_ID" FilterMode="InvalidChars" InvalidChars="'" />
                                                        <asp:TextBox ID="TextBox_IdTipoEstudio" runat="server" CssClass="form-control" PlaceHolder="Clave Estudio" style="display:none;"></asp:TextBox>
                                                        <asp:TextBox ID="TextBox_IdEstudioEditar" runat="server" CssClass="form-control" PlaceHolder="Clave Estudio" style="display:none;"></asp:TextBox>
                                                        <asp:TextBox ID="TextBox_EditarEstudio" runat="server" CssClass="form-control" PlaceHolder="Clave Estudio" style="display:none;"></asp:TextBox>
                                                    </div>
                                                    <div class="col-sm-12 col-md-6">
                                                        <label>Tipo Estudio</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup__TabCapturaEstudios_Tipo_Estudios" OnDataBinding="ASPxGridLookup__TabCapturaEstudios_Tipo_Estudios_DataBinding" Theme="iOS" runat="server" placeholder="Cliente" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{0}" Width="100%" ClientInstanceName="ClientGridLookup">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE" Width="100%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <asp:Button ID="Button_TabCapturaEstudio_CrearEstudio" OnClick="Button_TabCapturaEstudio_CrearEstudio_Click" runat="server" CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15" Style="display: inline-block; width: 100%; margin-top: 25px;" Text="Crear Estudio" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Cliente</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup_TabCapturaEstudios_Cliente" OnValueChanged="ASPxGridLookup_TabCapturaEstudios_Cliente_ValueChanged" OnDataBinding="ASPxGridLookup_TabCapturaEstudios_Cliente_DataBinding" Theme="iOS" runat="server" placeholder="Cliente" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{1} | {2}" Width="100%" ClientInstanceName="ClientGridLookup">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Width="30%" />
                                                                <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE" Width="40%" />
                                                                <dx:GridViewDataTextColumn Caption="Tipo" FieldName="TIPO_CLIENTE" Width="30%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </div>

                                                    <div class="col-sm-12 col-md-2">
                                                        <label>Paciente</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup__TabCapturaEstudios_Paciente" OnValueChanged="ASPxGridLookup__TabCapturaEstudios_Paciente_ValueChanged" OnDataBinding="ASPxGridLookup__TabCapturaEstudios_Paciente_DataBinding" Theme="iOS" runat="server" placeholder="Doctor" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{1}" Width="100%" ClientInstanceName="DoctorGridLookup">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Width="30%" />
                                                                <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE" Width="40%" />
                                                                <dx:GridViewDataTextColumn Caption="Edad" FieldName="EDAD" Width="30%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                            <ClientSideEvents EndCallback="OnEndCallback" />
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                    <asp:UpdatePanel ID="UpdatePanelEdadPaciente" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="col-sm-12 col-md-12">
                                                                <label>Edad</label>
                                                                <asp:TextBox ID="TextBox_TabCapturaEstudios_Edad" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" Width="100%"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaEstudios_Edad" runat="server" TargetControlID="TextBox_TabCapturaEstudios_Edad" FilterMode="InvalidChars" InvalidChars="'" />
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtenderNumeric_TextBox_TabCapturaEstudios_Edad" runat="server" TargetControlID="TextBox_TabCapturaEstudios_Edad" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                            <asp:Button ID="ButtonllenarEdad" OnClick="ButtonllenarEdad_Click" runat="server" CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15" Style="display: none; width: 100%; margin-top: 25px;" Text="Agregar Especimen" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Doctor</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup__TabCapturaEstudios_Doctor" OnValueChanged="ASPxGridLookup__TabCapturaEstudios_Doctor_ValueChanged" OnDataBinding="ASPxGridLookup__TabCapturaEstudios_Doctor_DataBinding" Theme="iOS" runat="server" placeholder="Doctor" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{1} | {2}" Width="100%" ClientInstanceName="DoctorGridLookup">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Width="30%" />
                                                                <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE" Width="40%" />
                                                                <dx:GridViewDataTextColumn Caption="Tipo" FieldName="TIPO_CLIENTE" Width="30%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </div>

                                                    <div class="col-sm-12 col-md-2">
                                                        <label>Patólogo Responable</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup__TabCapturaEstudios_Patologo" OnValueChanged="ASPxGridLookup__TabCapturaEstudios_Patologo_ValueChanged" OnDataBinding="ASPxGridLookup__TabCapturaEstudios_Patologo_DataBinding" Theme="iOS" runat="server" placeholder="Patólogo" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{0}" Width="100%" ClientInstanceName="DoctorGridLookup">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE" Width="100%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Fecha Alta</label>
                                                        <dx:ASPxDateEdit ID="ASPxDateEdit_Fecha_Alta" runat="server" EditFormat="Custom" Theme="iOS" Width="100%" Enabled="false">
                                                            <TimeSectionProperties>
                                                                <TimeEditProperties EditFormatString="hh:mm tt" />
                                                            </TimeSectionProperties>
                                                            <CalendarProperties>
                                                            </CalendarProperties>
                                                        </dx:ASPxDateEdit>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Fecha Toma</label>
                                                        <dx:ASPxDateEdit ID="ASPxDateEdit_Fecha_Toma" runat="server" OnValueChanged="ASPxDateEdit_Fecha_Toma_ValueChanged" EditFormat="Custom" Theme="iOS" Width="100%">
                                                            <TimeSectionProperties>
                                                                <TimeEditProperties EditFormatString="hh:mm tt" />
                                                            </TimeSectionProperties>
                                                            <CalendarProperties>
                                                            </CalendarProperties>
                                                        </dx:ASPxDateEdit>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Fecha Registro</label>
                                                        <dx:ASPxDateEdit ID="ASPxDateEdit_Fecha_Registro" runat="server" OnValueChanged="ASPxDateEdit_Fecha_Registro_ValueChanged" EditFormat="Custom" Theme="iOS" Width="100%">
                                                            <TimeSectionProperties>
                                                                <TimeEditProperties EditFormatString="hh:mm tt" />
                                                            </TimeSectionProperties>
                                                            <CalendarProperties>
                                                            </CalendarProperties>
                                                        </dx:ASPxDateEdit>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Procedencia</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup_TabCapturaEstudios_Tipo_Procedencia" OnValueChanged="ASPxGridLookup_TabCapturaEstudios_Tipo_Procedencia_ValueChanged" OnDataBinding="ASPxGridLookup_TabCapturaEstudios_Tipo_Procedencia_DataBinding" Theme="iOS" runat="server" placeholder="Cliente" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{0}" Width="100%" ClientInstanceName="ClientGridLookup">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE" Width="100%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                            <ClientSideEvents EndCallback="OnEndCallback" />
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                </div>
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel_Detalle_Procedencia" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <div class="row" style="float: right;">
                                                            <div class="col-sm-12 col-md-12">
                                                                <dx:ASPxLabel runat="server" ID="Label_TabCapturaEstudios_Detalle_Procedencia" Text="Detalle Procedencia"></dx:ASPxLabel>
                                                                <dx:ASPxGridLookup ID="ASPxGridLookup_TabCapturaEstudios_Detalle_Procedencia" OnValueChanged="ASPxGridLookup_TabCapturaEstudios_Detalle_Procedencia_ValueChanged" OnDataBinding="ASPxGridLookup_TabCapturaEstudios_Detalle_Procedencia_DataBinding" runat="server" placeholder="Cliente" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{0}" Width="100%" ClientInstanceName="ClientGridLookup">
                                                                    <ClearButton DisplayMode="OnHover"></ClearButton>
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE" Width="100%" />
                                                                    </Columns>
                                                                    <GridViewStyles>
                                                                        <AlternatingRow Enabled="true" />
                                                                        <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                        <Header Wrap="True" Font-Size="Small"></Header>
                                                                    </GridViewStyles>
                                                                    <GridViewProperties>
                                                                        <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                        <SettingsPager NumericButtonCount="3" />
                                                                    </GridViewProperties>
                                                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                                                </dx:ASPxGridLookup>

                                                            </div>
                                                            <asp:Button ID="ButtonllenarTipoProcedencia" OnClick="ButtonllenarTipoProcedencia_Click" runat="server" CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15" Style="display: none; width: 100%; margin-top: 25px;" Text="Agregar Especimen" />
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </div>
                                </div>
                                </section> </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!-- PESTAÑA CAPTURA DE ESTUDIOS END -->
                    <!-- PESTAÑA CAPTURA DE ESPECIMENES START -->
                    <div class="tab-pane fade show active" id="TabCapturaEspecimenes" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanel_TabCapturaEspecimenes" runat="server" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ASPxGridLookup__TabCapturaEspecimenes_Especimen"
                                    EventName="ValueChanged" />
                            </Triggers>
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Captura de Especimenes</h3>
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">

                                            <div class="form-group" runat="server" id="GridEspecimenes">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Especimenes</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup__TabCapturaEspecimenes_Especimen" OnDataBinding="ASPxGridLookup__TabCapturaEspecimenes_Especimen_DataBinding" Theme="iOS" runat="server" placeholder="Especimen" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{1}" Width="100%" ClientInstanceName="EspecimenGridLookup">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Width="10%" />
                                                                <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE" Width="30%" />
                                                                <dx:GridViewDataTextColumn Caption="Precio Unitario" FieldName="PRECIO_UNITARIO" Width="30%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                            <ClientSideEvents EndCallback="OnEndCallback" />
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                    <div class="col-sm-12 col-md-2">
                                                        
                                                    </div>
                                                    <div class="col-sm-12 col-md-2">
                                                       
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <asp:Button ID="Button_TabCapturaEstudio_AgregarEspecimen" OnClick="Button_TabCapturaEstudio_AgregarEspecimen_Click" runat="server" CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15" Style="display: inline-block; width: 100%; margin-top: 25px;" Text="Agregar Especimen" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" runat="server" id="GridViewEspecimenes">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-12">
                                                        <dx:ASPxGridView ID="ASPxGridView_TabCapturaEspecimenes_Especimenes" OnRowCommand="ASPxGridView_TabCapturaEspecimenes_Especimenes_RowCommand" OnRowUpdating="ASPxGridView_TabCapturaEspecimenes_Especimenes_RowUpdating" OnDataBinding="ASPxGridView_TabCapturaEspecimenes_Especimenes_DataBinding" Theme="Material" runat="server" AutoGenerateColumns="False" KeyFieldName="ID" Width="100%" EnableCallBacks="false" EnableRowsCache="false">
                                                            <Columns>

                                                                <dx:GridViewDataColumn Caption="Eliminar" Name="Buttons" Width="100">
                                                                    <EditFormSettings Visible="False" />
                                                                    <DataItemTemplate>
                                                                        <center>
                                                                            <asp:LinkButton ID="ASPxButton20" runat="server" CommandName="ELIMINAR" ToolTip="Eliminar número de parte">
                                                                                <i class="fa fa-times-rectangle-o fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataColumn>

                                                                <dx:GridViewDataTextColumn FieldName="ID" Caption="ID" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Nombre" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                                <dx:GridViewDataSpinEditColumn FieldName="PRECIO_VENTA" Caption="Precio Venta" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" >
                                                                    <PropertiesSpinEdit MinValue="0" MaxValue="600000000000">
                                                                        <ValidationSettings Display="Dynamic" RequiredField-IsRequired="true" />
                                                                    </PropertiesSpinEdit>
                                                                </dx:GridViewDataSpinEditColumn>

                                                                <dx:GridViewDataSpinEditColumn FieldName="CANTIDAD" Caption="Cantidad" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                                <PropertiesSpinEdit MinValue="1" MaxValue="600000000000">
                                                                        <ValidationSettings Display="Dynamic" RequiredField-IsRequired="true" />
                                                                    </PropertiesSpinEdit>
                                                                </dx:GridViewDataSpinEditColumn>

                                                                <dx:GridViewDataSpinEditColumn FieldName="CANTIDAD_ETIQUETAS" Caption="Cantidad etiquetas" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                                <PropertiesSpinEdit MinValue="1" MaxValue="600000000000">
                                                                        <ValidationSettings Display="Dynamic" RequiredField-IsRequired="true" />
                                                                    </PropertiesSpinEdit>
                                                                </dx:GridViewDataSpinEditColumn>

                                                            </Columns>
                                                            <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Auto" VerticalScrollableHeight="400" ShowFooter="true" />
                                                            <Settings ShowGroupPanel="true" />
                                                            <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                            <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                            <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                            <SettingsEditing Mode="Batch" BatchEditSettings-EditMode="Cell" />
                                                            <Styles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small" />
                                                                <Header Wrap="True" Font-Size="Small" />
                                                            </Styles>
                                                        </dx:ASPxGridView>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row" style="float: right;">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-top: 5%;">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4"></div>
                                                    <div class="col-sm-12 col-md-4"></div>
                                                    <div class="col-sm-12 col-md-2">
                                                    <asp:Label runat="server" ID="Label_TabCapturaEstudios_Anticipo" Text="Anticipo" Visible="true"></asp:Label>
                                                    <asp:TextBox ID="TextBox_TabCapturaEstudios_Anticipo" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="$"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaEstudios_Anticipo" runat="server" TargetControlID="TextBox_TabCapturaEstudios_Anticipo" FilterMode="InvalidChars" InvalidChars="'" />
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtenderNumeric_TextBox_TabCapturaEstudios_Anticipo" runat="server" TargetControlID="TextBox_TabCapturaEstudios_Anticipo" FilterType="Custom, Numbers" ValidChars="." />
                                                    </div>
                                                    <div class="col-sm-12 col-md-2">
                                                    <asp:Label runat="server" ID="Label_TabCapturaEstudios_Concepto" Text="Concepto" Visible="true"></asp:Label>
                                                     <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="DropDownListConceptoRC" runat="server" OnDataBinding="DropDownListConceptoRC_DataBinding" CssClass="form-control rounded-0" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-top: 5%;">
                                                <div class="row">
                                                    
                                                    <div class="col-sm-12 col-md-10"></div>
                                                    <div class="col-sm-12 col-md-2">
                                                        <label id="LabelEnviarRevision" runat="server" class="d-flex align-items-center justify-content-between">
                                                            <span>Enviar a revisión</span>
                                                            <div class="u-check">
                                                                <input id="InputEnviarRevision" runat="server" class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="radGroup3_1" checked="" type="checkbox">
                                                                <div class="u-check-icon-radio-v8">
                                                                    <i class="fa" data-check-icon="&#xf00c"></i>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>

                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row" style="float: right;">
                                                    <div class="col-sm-12 col-md-6">
                                                        <asp:Button ID="Button_Cancelar_Estudio" OnClick="Button_Cancelar_Estudio_Click" runat="server" CssClass="btn btn-md u-btn-red g-mr-10 g-mb-15" Style="display: inline-block; width: 100%; margin-top: 25px;" Text="Cancelar Captura"  Visible="True"/>
                                                    </div>
                                                    <div class="col-sm-12 col-md-6">
                                                        <asp:Button ID="Button_Guardar_Estudio" OnClick="Button_Guardar_Estudio_Click" runat="server" CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15" Style="display: inline-block; width: 100%; margin-top: 25px;" Text="Guardar Estudio" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                </div>
                                </section> </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!-- PESTAÑA CAPTURA DE ESPECIMENES END -->
                </div>
                <!-- TAB PANES END -->
            </div>
        </div>
    </div>
    <!-- CONTAINER END -->
</asp:Content>
