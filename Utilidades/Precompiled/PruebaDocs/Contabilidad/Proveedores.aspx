﻿<%@ page title="Gestión de Proveedores" language="C#" masterpagefile="~/Layout.master" autoeventwireup="true" inherits="Contabilidad_Proveedores, App_Web_3suzqkde" %>

<asp:Content ID="Content_Principal" ContentPlaceHolderID="ContentPlaceHolder_Principal"
    runat="Server">
    <!-- UPDATEPROGRESS START -->
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div class="image-loading-container">
                <asp:Image ID="imgUpdateProgress" runat="server" CssClass="image-loading" ImageUrl="~/Public/Images/load.gif"
                    AlternateText="Loading ..." ToolTip="Loading ..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <!-- UPDATEPROGRESS END -->
    <!-- CONTAINER START -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <!-- NAV START -->
                <ul class="nav u-nav-v5-1" role="tablist" data-target="nav-5-1-default-hor-left-icons"
                    data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-lightgray">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#TabCapturaProveedores"
                        role="tab" onclick="CambiarTabCapturaProveedores()"><i class="et-icon-clipboard u-tab-line-icon-pro g-mr-3">
                        </i>Gestión de Proveedores </a></li>
                </ul>
                <!-- NAV END -->
                <!-- TAB PANES START -->
                <div id="nav-5-1-default-hor-left-icons" class="tab-content g-pt-20">
                    <!-- PESTAÑA CAPTURA DE USUARIOS START -->
                    <div class="tab-pane fade show active" id="TabCapturaProveedores" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanel_TabCapturaProveedores" runat="server" ChildrenAsTriggers="false"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Captura de Proveedores</h3>
                                        <asp:LinkButton ID="LinkButton_TabCapturaProveedores_DescargarExcel" OnClick="LinkButton_TabCapturaProveedores_DescargarExcel_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-bluegray g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Descargar Excel</span>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton_TabCapturaProveedores_Actualizar" OnClick="LinkButton_TabCapturaProveedores_Actualizar_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-teal g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Actualizar</span>
                                        </asp:LinkButton>
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Razón Social</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaProveedores_RazonSocial" runat="server" CssClass="form-control" PlaceHolder="Nombre del proveedor"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_RazonSocial" runat="server" TargetControlID="TextBox_TabCapturaProveedores_RazonSocial" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Nombre Comercial</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaProveedores_NombreComercial" runat="server" CssClass="form-control" PlaceHolder="Nombre comercial"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_NombreComercial" runat="server" TargetControlID="TextBox_TabCapturaProveedores_NombreComercial" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>RFC</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaProveedores_RFC" runat="server" CssClass="form-control" PlaceHolder="RFC"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_RFC" runat="server" TargetControlID="TextBox_TabCapturaProveedores_RFC" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                     <div class="col-sm-12 col-md-4">
                                                            <label>Estado</label>
                                                            <asp:TextBox ID="TextBox_TabCapturaProveedores_Estado" runat="server" CssClass="form-control" PlaceHolder="Estado"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_Estado" runat="server" TargetControlID="TextBox_TabCapturaProveedores_Estado" FilterMode="InvalidChars" InvalidChars="'" />
                                                        </div>
                                                        <div class="col-sm-12 col-md-4">
                                                            <label>Municipio</label>
                                                            <asp:TextBox ID="TextBox_TabCapturaProveedores_Municipio" runat="server" CssClass="form-control" PlaceHolder="Municipio"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_Municipio" runat="server" TargetControlID="TextBox_TabCapturaProveedores_Municipio" FilterMode="InvalidChars" InvalidChars="'" />
                                                        </div>
                                                        <div class="col-sm-12 col-md-4">
                                                            <label>Colonia</label>
                                                            <asp:TextBox ID="TextBox_TabCapturaProveedores_Colonia" runat="server" CssClass="form-control" PlaceHolder="Colonia"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_Colonia" runat="server" TargetControlID="TextBox_TabCapturaProveedores_Colonia" FilterMode="InvalidChars" InvalidChars="'" />
                                                        </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                     <div class="col-sm-12 col-md-4">
                                                            <label>Calle</label>
                                                            <asp:TextBox ID="TextBox_TabCapturaProveedores_Calle" runat="server" CssClass="form-control" PlaceHolder="Calle"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_Calle" runat="server" TargetControlID="TextBox_TabCapturaProveedores_Calle" FilterMode="InvalidChars" InvalidChars="'" />
                                                        </div>
                                                        <div class="col-sm-12 col-md-4">
                                                            <label>Número Exterior</label>
                                                            <asp:TextBox ID="TextBox_TabCapturaProveedores_NumExt" runat="server" CssClass="form-control" PlaceHolder="Número Exterior"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_NumExt" runat="server" TargetControlID="TextBox_TabCapturaProveedores_NumExt" FilterMode="InvalidChars" InvalidChars="'" />
                                                        </div>
                                                        <div class="col-sm-12 col-md-4">
                                                            <label>Número Interior</label>
                                                            <asp:TextBox ID="TextBox_TabCapturaProveedores_NumInt" runat="server" CssClass="form-control" PlaceHolder="Número Interior"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_NumInt" runat="server" TargetControlID="TextBox_TabCapturaProveedores_NumInt" FilterMode="InvalidChars" InvalidChars="'" />
                                                        </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Teléfono propio</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaProveedores_TelPropio" runat="server" CssClass="form-control" PlaceHolder="Número Interior"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_TelPropio" runat="server" TargetControlID="TextBox_TabCapturaProveedores_TelPropio" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Teléfono de oficina</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaProveedores_Oficina" runat="server" CssClass="form-control" PlaceHolder="Número Interior"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_Oficina" runat="server" TargetControlID="TextBox_TabCapturaProveedores_Oficina" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Correo electrónico</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaProveedores_CorreoElectronico" runat="server" CssClass="form-control" PlaceHolder="Correo electrónico"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_CorreoElectronico" runat="server" TargetControlID="TextBox_TabCapturaProveedores_CorreoElectronico" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <div class="row">

                                                <div class="col-sm-12 col-md-4">
                                                    <label>Días de Crédito</label>
                                                    <asp:TextBox ID="TextBox_TabCapturaProveedores_DiasCredito" runat="server" CssClass="form-control" MaxLength="10" PlaceHolder="0"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaProveedores_DiasCredito" runat="server" TargetControlID="TextBox_TabCapturaProveedores_DiasCredito" FilterMode="InvalidChars" InvalidChars="'" />
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_Numeros_TextBox_TabCapturaProveedores_DiasCredito" runat="server" TargetControlID="TextBox_TabCapturaProveedores_DiasCredito" FilterType="Custom, Numbers" ValidChars="." />
                                                </div>

                                                <div class="col-sm-12 col-md-2"></div>

                                                <div class="col-sm-12 col-md-6 text-right">
                                                    <asp:Button ID="Button1" OnClick="Button_TabCapturaProveedores_Guardar_OnClick" runat="server" CssClass="btn btn-info" Text="Guardar Proveedor" Style="display: inline-block; margin-top: 20px;" />
                                                </div>
                                            </div>
                                        </div>
                                        </div>

                                        

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <dx:ASPxGridView ID="ASPxGridView_TabCapturaProveedores_ProveedoresActivos" OnRowCommand="ASPxGridView_TabCapturaProveedores_ProveedoresActivos_OnRowCommand" OnDataBinding="ASPxGridView_TabCapturaProveedores_ProveedoresActivos_OnDataBinding" Theme="Material" runat="server" AutoGenerateColumns="False" KeyFieldName="ID" Width="100%">
                                                        <Columns>

                                                            <dx:GridViewDataColumn Caption="Editar" Name="Buttons" Width="100">
                                                                <EditFormSettings Visible="False" />
                                                                <DataItemTemplate>
                                                                    <center>
                                                                            <asp:LinkButton ID="ASPxButton2" runat="server" CommandName="EDITAR" ToolTip="Editar número de parte">
                                                                                <i class="fa fa-edit fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="Eliminar" Name="Buttons" Width="100">
                                                                <EditFormSettings Visible="False" />
                                                                <DataItemTemplate>
                                                                    <center>
                                                                            <asp:LinkButton ID="ASPxButton20" runat="server" CommandName="ELIMINAR" ToolTip="Eliminar número de parte">
                                                                                <i class="fa fa-times-rectangle-o fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>

                                                            <dx:GridViewDataTextColumn FieldName="ID" Caption="ID" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                                <CellStyle HorizontalAlign="Center"></CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="RAZON_SOCIAL" Caption="Razón Social" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                            <dx:GridViewDataTextColumn FieldName="NOMBRE_COMERCIAL" Caption="Nombre Comercial" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="RFC" Caption="RFC" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="ESTADO" Caption="Estado" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="MUNICIPIO" Caption="Municipio" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="COLONIA" Caption="Colonia" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="CALLE" Caption="Calle" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="NUM_EXT" Caption="Número Exterior" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="NUM_INT" Caption="Número Intetior" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="TEL_PROPIO" Caption="Teléfono Propio" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="TEL_OFICINA" Caption="Teléfono de oficina" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="EMAIL" Caption="Correo Electrónico" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="DIAS_CREDITO" Caption="Días de crédito" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                        </Columns>
                                                        <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Auto" VerticalScrollableHeight="400" ShowFooter="true" />
                                                        <Settings ShowGroupPanel="true" />
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                        <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                        <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                        <Styles>
                                                            <AlternatingRow Enabled="true" />
                                                            <Cell Wrap="True" Font-Size="Small" />
                                                            <Header Wrap="True" Font-Size="Small" />
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter_ASPxGridView_TabCapturaProveedores_ProveedoresActivos" runat="server" GridViewID="ASPxGridView_TabCapturaProveedores_ProveedoresActivos" />
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                </section> </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="LinkButton_TabCapturaProveedores_DescargarExcel" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <!-- PESTAÑA CAPTURA DE USUARIOS END -->
                </div>
                <!-- TAB PANES END -->
            </div>
        </div>
    </div>
    <!-- CONTAINER END -->
    <!-- MODAL EDITAR USUARIO START -->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0 dxpc-content-wi"
        Width="900px" ID="Modal_EditarProveedor" ClientInstanceName="Modal_EditarProveedor"
        runat="server" ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true"
        Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        AllowDragging="True" PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False"
        AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_ModalEditarProveedor" runat="server" ChildrenAsTriggers="false"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <!-- Info Outline Panel-->
                        <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                            <i class="fa fa-tasks g-font-size-default g-mr-5"></i>Editar Proveedor
                        </h3>
                        <div class="card-block">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label>
                                            Razón social</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_RazonSocial" runat="server" CssClass="form-control"
                                            PlaceHolder="Razón Social"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_RazonSocial"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_RazonSocial" FilterMode="InvalidChars"
                                            InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <label>
                                            Nombre comercial</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_NombreComercial" runat="server" CssClass="form-control"
                                            PlaceHolder="Nombre comercial"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_NombreComercial"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_NombreComercial"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <label>
                                            RFC</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_RFC" runat="server" CssClass="form-control"
                                            PlaceHolder="RFC"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_RFC"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_RFC" FilterMode="InvalidChars"
                                            InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label>
                                            Estado</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_Estado" runat="server" CssClass="form-control"
                                            PlaceHolder="Estado"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_Estado"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_Estado" FilterMode="InvalidChars"
                                            InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label>
                                            Municipio</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_Municipio" runat="server" CssClass="form-control"
                                            PlaceHolder="Municipio"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_Municipio"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_Municipio" FilterMode="InvalidChars"
                                            InvalidChars="'" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <label>
                                            Colonia</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_Colonia" runat="server" CssClass="form-control"
                                            PlaceHolder="Colonia"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_Colonia"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_Colonia" FilterMode="InvalidChars"
                                            InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label>
                                            Calle</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_Calle" runat="server" CssClass="form-control"
                                            PlaceHolder="Calle"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_Calle"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_Calle" FilterMode="InvalidChars"
                                            InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label>
                                            Número Exterior</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_NumExt" runat="server" CssClass="form-control"
                                            PlaceHolder="Número Exterior"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_NumExt"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_NumExt" FilterMode="InvalidChars"
                                            InvalidChars="'" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <label>
                                            Número Interior</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_NumInt" runat="server" CssClass="form-control"
                                            PlaceHolder="Número Interior"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_NumInt"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_NumInt" FilterMode="InvalidChars"
                                            InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label>
                                            Teléfono propio</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_TelPropio" runat="server" CssClass="form-control"
                                            PlaceHolder="Teléfono Propio"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_TelPropio"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_TelPropio" FilterMode="InvalidChars"
                                            InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label>
                                            Teléfono de oficina</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_TelOficina" runat="server" CssClass="form-control"
                                            PlaceHolder="Teléfono oficina"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_TelOficina"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_TelOficina" FilterMode="InvalidChars"
                                            InvalidChars="'" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label>
                                            Correo electrónico</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_CorreoElectronico" runat="server"
                                            CssClass="form-control" PlaceHolder="Correo electrónico"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarProveedor_CorreoElectronico"
                                            runat="server" TargetControlID="TextBox_Modal_EditarProveedor_CorreoElectronico"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label>
                                            Días de Crédito</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarProveedor_DiasCredito" runat="server" CssClass="form-control"
                                            MaxLength="10" PlaceHolder="0"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox_Modal_EditarProveedor_DiasCredito"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="TextBox_Modal_EditarProveedor_DiasCredito"
                                            FilterType="Custom, Numbers" ValidChars="." />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 100px;">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-right">
                                        <asp:Button ID="Button_ModalEditarProveedor_GuardarCambios" OnClick="Button_ModalEditarProveedor_GuardarCambios_OnClick"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15" Text="Guardar Cambios" />
                                        <asp:Button ID="Button_ModalEditarProveedor_CerrarModal" OnClick="Button_ModalEditarProveedor_CerrarModal_OnClick"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15"
                                            Text="Cancelar" />
                                    </div>
                                </div>
                            </div>
                            <!-- CAMPOS OCULTOS START -->
                            <asp:TextBox ID="TextBox_ModalEditarProveedor_IdProveedor" runat="server" CssClass="hidden-control"></asp:TextBox>
                            <asp:HiddenField ID="HiddenField_ModalEditarProveedor_IdProveedor" runat="server" />
                            <!-- CAMPOS OCULTOS END -->
                        </div>
                        <!-- End Info Outline Panel-->
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!-- MODAL EDITAR USUARIO END -->
</asp:Content>
