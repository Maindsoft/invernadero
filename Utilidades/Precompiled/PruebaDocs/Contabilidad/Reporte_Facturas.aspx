﻿<%@ page title="Reporte Facturas" language="C#" masterpagefile="~/Layout.master" autoeventwireup="true" inherits="Contabilidad_Reporte_Facturas, App_Web_3suzqkde" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content_Principal" ContentPlaceHolderID="ContentPlaceHolder_Principal" runat="Server">
<%--<script type="text/javascript">    var plugin_path = '../Scripts/plugins/';</script>--%>
    
   
        <div class="container-fluid">
        <div class="row">
        <div class="col-sm-12 col-md-12">
        <asp:UpdatePanel ID="UpdatePanelReporte" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                       <div class="card card-outline-info rounded-0">
                            <header class="card-header bg-info">
                                <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Reporte Facturas</h3>
                                <ul class="options pull-right list-inline"  style="display:none;">    
                                    <li><asp:LinkButton ID="ButtonEnviarCorreoCPC" title="Enviar por correo" data-toggle="tooltip" data-placement="bottom" runat="server" CssClass="fa fa-paper-plane fa-lg tip" /></li>                                        
                                    <li><asp:LinkButton ID="ButtonDescargarExcelECG" title="Descargar a Excel" data-toggle="tooltip" data-placement="bottom" runat="server" CssClass="fa fa fa-download fa-lg tip" /></li>
                                </ul>                                        
                            </header>
                            <div class="panel-body" style="padding: 0px;">
                                <div class="row" style="padding: 10px;">
                                    <div class="col-sm-12">
                                        <div>
                                            <label>Celda editable</label>
                                            <div title="Campos que se pueden editar, depende del origen del componente" data-toggle="tooltip" class="tip" data-placement="bottom" style="height:20px; width:30px; background-color:#FCF8E3; border:1px solid #ddd;"></div>
                                        </div>
                                    </div><!--.col-sm-3-->
                                </div>
                                <dx:ASPxGridView ID="ASPxGridViewPorFactura" OnDataBinding="ASPxGridViewPorFactura_OnDataBinding" OnHtmlDataCellPrepared="ASPxGridViewPorFactura_OnHtmlDataCellPrepared" OnRowUpdating="ASPxGridViewPorFactura_RowUpdating" Theme="Moderno" runat="server" AutoGenerateColumns="False" KeyFieldName="ID_VENTA" Width="100%" OnRowCommand="ASPxGridViewPorFactura_OnRowCommand">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="ID_VENTA" AdaptivePriority="1" PropertiesTextEdit-DisplayFormatInEditMode="false" Caption="Id" SettingsHeaderFilter-Mode="CheckedList" Settings-ShowFilterRowMenu="true" Settings-AllowHeaderFilter="true"  />
                                        <dx:GridViewDataTextColumn FieldName="DOCUMENTO" AdaptivePriority="1" PropertiesTextEdit-DisplayFormatInEditMode="false" Caption="Tipo Documento" SettingsHeaderFilter-Mode="CheckedList" Settings-ShowFilterRowMenu="true" Settings-AllowHeaderFilter="true"  />
                                        <dx:GridViewDataTextColumn FieldName="NOMBRE" AdaptivePriority="2" Caption="Cliente" SettingsHeaderFilter-Mode="CheckedList" Settings-ShowFilterRowMenu="False" Settings-AllowHeaderFilter="true"  />
                                        <dx:GridViewDataTextColumn FieldName="RFC" AdaptivePriority="2" Caption="RFC" SettingsHeaderFilter-Mode="CheckedList" Settings-ShowFilterRowMenu="true" Settings-AllowHeaderFilter="true"  />
                                        <dx:GridViewDataTextColumn FieldName="ESTATUS" AdaptivePriority="1" Caption="Estatus" SettingsHeaderFilter-Mode="CheckedList" Settings-ShowFilterRowMenu="true" Settings-AllowHeaderFilter="true"  />
                                        <dx:GridViewDataTextColumn FieldName="CREADO" AdaptivePriority="1" Caption="Creado por" SettingsHeaderFilter-Mode="CheckedList" Settings-ShowFilterRowMenu="true" Settings-AllowHeaderFilter="true" />
                                        <%--<dx:GridViewDataTextColumn FieldName="ID_PEDIDO" AdaptivePriority="2" Caption="Id Pedido" SettingsHeaderFilter-Mode="CheckedList" Settings-ShowFilterRowMenu="False" Settings-AllowHeaderFilter="true" />--%>
                                        <dx:GridViewDataSpinEditColumn  FieldName="CLAVE_ESTUDIO" Caption="Clave Estudio"  SettingsHeaderFilter-Mode="CheckedList" Settings-ShowFilterRowMenu="False" Settings-AllowHeaderFilter="true" >
                                        </dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataDateColumn FieldName="FECHA_CREO" Caption="Fecha creo" Settings-AllowHeaderFilter="True"  AdaptivePriority="2" Settings-ShowFilterRowMenu="True" />
                                        <dx:GridViewDataDateColumn FieldName="FECHA_APLICACION" Caption="Fecha Aplicación" Settings-AllowHeaderFilter="True"  AdaptivePriority="2" Settings-ShowFilterRowMenu="True" />
                                        <dx:GridViewDataTextColumn FieldName="DESCRIPCION_MONEDA" AdaptivePriority="2" Caption="Moneda" SettingsHeaderFilter-Mode="CheckedList" Settings-ShowFilterRowMenu="False" Settings-AllowHeaderFilter="true" />
                                        <dx:GridViewDataSpinEditColumn FieldName="TOTAL_PESOS" Caption="Total Pesos" SettingsHeaderFilter-Mode="CheckedList" Settings-ShowFilterRowMenu="False" Settings-AllowHeaderFilter="true" AdaptivePriority="2" >
                                            <PropertiesSpinEdit DisplayFormatString="c" DecimalPlaces="2" />
                                        </dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataColumn Caption="Detalles" AdaptivePriority="1"   Name="Buttons" Visible="false">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="ASPxButton31" runat="server" CommandName="verDetalles" ToolTip="Ver detalle" CommandArgument="A"  RenderMode="Link" EnableTheming="False">
                                                    <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                                                </asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="Ver PDF" AdaptivePriority="1"  Name="Buttons" Visible="false">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="ASPxButton31" runat="server" CommandName="verPDF" ToolTip="Ver PDF" CommandArgument="A"  RenderMode="Link" EnableTheming="False">
                                                    <i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i>
                                                </asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataColumn>
                                        
                                       
                                        <dx:GridViewDataColumn Caption="Enviar por Correo" AdaptivePriority="1"  Name="Buttons" Visible="false">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="ASPxButton31" runat="server" CommandName="enviarMail" ToolTip="Enviar por Correo" CommandArgument="A"  RenderMode="Link" EnableTheming="False">
                                                    <i class="fa fa-share fa-2x" aria-hidden="true"></i>
                                                </asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataColumn>
                                         <dx:GridViewDataColumn Caption="Cancelar Factura" AdaptivePriority="1"  Name="Buttons" >
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="ASPxButton31" runat="server" CommandName="cancelarFactura" ToolTip="Cancelar Factura" CommandArgument="A"  RenderMode="Link" EnableTheming="False">
                                                    <i class="fa fa-times-circle-o fa-2x" aria-hidden="true"></i>
                                                </asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="Convertir a Factura" AdaptivePriority="1"  Name="Buttons" >
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="ASPxButton31" runat="server" CommandName="convertirFactura" ToolTip="Convertir nota de remisión a factura" CommandArgument="A"  RenderMode="Link" EnableTheming="False">
                                                    <i class="fa fa-refresh fa-2x" aria-hidden="true"></i>
                                                </asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="Ver Documento" AdaptivePriority="1"  Name="Buttons" Visible="True">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="ASPxButton31" runat="server" CommandName="VerFactura" ToolTip="Ver PDF" CommandArgument="A"  RenderMode="Link" EnableTheming="False">
                                                    <i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i>
                                                </asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataColumn>
                                         <dx:GridViewDataColumn Caption="XML" AdaptivePriority="1"  Name="Buttons" >
                                            <DataItemTemplate>
                                                <asp:HyperLink ID="RUTA_DOCUMENTO" runat="server" ToolTip="Ver XML" NavigateUrl='<%# Eval("RUTA", "~/{0}") %>' Target="_blank"><i class="fa fa-file fa-2x" aria-hidden="true"></i></asp:HyperLink>
                                            </DataItemTemplate>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="Copiar a una factura" AdaptivePriority="1"  Name="Buttons" Visible="false">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="ASPxButton31" runat="server" CommandName="copiarFactura" ToolTip="Copiar datos de la factura en una nueva" CommandArgument="A" RenderMode="Link" EnableTheming="False">
                                                    <i class="fa fa-files-o fa-2x" aria-hidden="true"></i>
                                                </asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                    <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Auto"  VerticalScrollableHeight="400" ShowFooter="true" />
                                    <SettingsAdaptivity AdaptivityMode="HideDataCells"  AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                    <SettingsPager PageSize="50" ShowNumericButtons="false" />
                                    <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                     <SettingsEditing Mode="Batch" BatchEditSettings-EditMode="Cell" />
                                    <TotalSummary >
                                        <dx:ASPxSummaryItem FieldName="TOTAL_PESOS" DisplayFormat="{0:c}" ShowInColumn="TOTAL_PESOS"  SummaryType="Sum" />
                                        <dx:ASPxSummaryItem FieldName="TOTAL_DLL" DisplayFormat="{0:c}" SummaryType="Sum" />
                                    </TotalSummary>
                                    <Styles>
                                        <AlternatingRow Enabled="true" />
                                        <Cell Wrap="True" Font-Size="Small" />
                                        <Header Wrap="True" Font-Size="Small" />
                                    </Styles>
                                </dx:ASPxGridView>
                                <dx:ASPxGridViewExporter ID="gridExportPorFactura" runat="server" GridViewID="ASPxGridViewPorFactura" />
                            </div>
                        </div>
            </ContentTemplate>
           
        </asp:UpdatePanel>
        </div>
        </div>
        </div>
       
        <!----- MODAL CONVERTIR FACTURA START ----->
        <div class="modal fade" id="modalConvertirFactura" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <asp:UpdatePanel ID="UpdatePanelConvertirFactura" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-content">
                            <div class="modal-header">
                               <span style="padding:10px; margin-left:-10px; margin-right:15px;"><asp:ImageButton ID="ImageButton2" CssClass="close" Width="50" Height="50" ImageUrl="~/Public/Images/cancel.png" data-dismiss="modal" aria-hidden="true" runat="server" /></span>
                                <h4 class="modal-title"><asp:Label ID="lblModalTitleConvertir" runat="server" Text=""></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group"">
                                            <label>Cliente:</label>
                                            <asp:TextBox ID="TextBoxClienteConvertir" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div><!--row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"">
                                            <label>Folio:</label>
                                            <asp:TextBox ID="TextBoxFolioConvertir" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group"">
                                            <label>Fecha:</label>
                                            <asp:TextBox ID="TextBoxFechaConvertir" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div><!--row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"">
                                            <label>Estatus:</label>
                                            <asp:TextBox ID="TextBoxEstatusConvertir" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group"">
                                            <label>Moneda:</label>
                                            <asp:TextBox ID="TextBoxMonedaConvertir" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div><!--row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group"">
                                            <label>Subtotal:</label>
                                            <asp:TextBox ID="TextBoxSubtotalConvertir" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group"">
                                            <label>IVA:</label>
                                            <asp:TextBox ID="TextBoxIvaConvertir" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group"">
                                            <label>Total:</label>
                                            <asp:TextBox ID="TextBoxTotalConvertir" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div><!--row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"">
                                            <label>Forma de Pago:</label>
                                            <asp:DropDownList runat="server" ID="DropdownListClaveMetodoPago" DataTextField="METODO" DataValueField="ID_METODO_PAGO" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group"">
                                            <label>Uso del CFDI:</label>
                                            <asp:DropDownList runat="server" ID="DropdownListUdoCFDI" DataTextField="DESCRIPCION_USO_CFDI" DataValueField="CLAVE_USO_CFDI" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                             <label>Clave Método de Pago SAT:</label>
                                             <asp:DropDownList runat="server" ID="DropdownListClaveMetodoPago2" DataTextField="METODO" DataValueField="CALVE_METODO_PAGO" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                               
                            </div><!--ModalBody-->
                            <div class="modal-footer">
                                <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                <asp:Button runat="server" CssClass="btn btn-success" ID="ButtonConvertirFactura" OnClick="ButtonConvertirFactura_OnClick" Text="Convertir en Factura Directo" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
       
        <!----- MODAL ENVIAR POR CORREO START ----->
        <div class="modal fade" id="modalEnviarCorreo"   data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md" >
                <asp:UpdatePanel ID="UpdatePanelModalEnviarCorreo" style="z-index:-1" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Panel3" style="z-index:-1" runat="server">
                            <div class="modal-content panel-danger" ">
                                <div class="modal-header">
                                    <asp:ImageButton ID="ImageButton1" CssClass="close" Width="15" Height="15" ImageUrl="~/Images/cancel.png" data-dismiss="modal" aria-hidden="true" runat="server" />
                                    <h4 class="modal-title"><asp:Label ID="Label1" runat="server" Text="Enviar por correo"></asp:Label></h4>
                                </div>
                                <div class="modal-body">
                                    <asp:Label runat="server" ID="LabelIDVentaCorreo" Visible="false"></asp:Label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group" style="z-index:999999" >
                                                <label>Para*:</label>
                                                <asp:TextBox ID="TextBoxDestinario" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="TextBoxDestinario" InvalidChars="'" FilterMode="InvalidChars" runat="server"></asp:FilteredTextBoxExtender>
                                                <asp:AutoCompleteExtender  ID="AutoCompleteExtender2" runat="server"
                                                    ServiceMethod="AutoCompleteCorreo"
                                                    ServicePath="~/AutoComplete.asmx"
                                                    MinimumPrefixLength="1"
                                                    CompletionInterval="100"
                                                    EnableCaching="false"
                                                    CompletionSetCount="10"
                                                    TargetControlID="TextBoxDestinario" 
                                                    FirstRowSelected="false"
                                                    DelimiterCharacters=";"
                                                    CompletionListCssClass="CompletionList"
                                                    CompletionListItemCssClass="CompletionListItem" 
                                                    CompletionListHighlightedItemCssClass="CompletionListHighlightedItem">
                                                </asp:AutoCompleteExtender>
                                            </div>
                                        </div>
                                    </div><!--row--> 
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group"">
                                                <label>CC:</label>
                                                <asp:TextBox ID="TextBoxCC" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TextBoxCC" InvalidChars="'" FilterMode="InvalidChars" runat="server"></asp:FilteredTextBoxExtender>
                                                <asp:AutoCompleteExtender  ID="AutoCompleteExtender3" runat="server"
                                                    ServiceMethod="AutoCompleteCorreo"
                                                    ServicePath="~/AutoComplete.asmx"
                                                    MinimumPrefixLength="1"
                                                    CompletionInterval="100"
                                                    EnableCaching="false"
                                                    CompletionSetCount="10"
                                                    TargetControlID="TextBoxCC" 
                                                    FirstRowSelected="false"
                                                    DelimiterCharacters=";"
                                                    CompletionListCssClass="CompletionList"
                                                    CompletionListItemCssClass="CompletionListItem" 
                                                    CompletionListHighlightedItemCssClass="CompletionListHighlightedItem">
                                                </asp:AutoCompleteExtender>
                                            </div>
                                        </div>
                                    </div><!--row-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group"">
                                                <label>Asunto*:</label>
                                                <asp:TextBox ID="TextBoxAsunto" Text="Envio de factura" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="TextBoxAsunto" InvalidChars="'" FilterMode="InvalidChars" runat="server"></asp:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </div><!--row--> 
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group"">
                                                <label>Mensaje:</label>
                                                <asp:TextBox ID="TextBoxMensaje" runat="server" TextMode="MultiLine" Rows="6" CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="TextBoxMensaje" InvalidChars="'" FilterMode="InvalidChars" runat="server">
                                                </asp:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </div><!--row-->     
                                    <div runat="server" id="divErrorEnviarMail" visible="false" class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <i class="fa fa-info-circle"></i><strong>  <asp:Label runat="server" ID="LabelErrorCorreo"></asp:Label></strong> 
                                    </div>                       
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                                    <asp:Button runat="server" ID="ButtonEnviar" ValidationGroup="enviarCorreo" CausesValidation="true" OnClick="ButtonEnviar_OnClick" CssClass="btn btn-success" Text="Aceptar" />
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <!----- MODAL ENVIAR POR CORREO END ----->
        <!----- MODAL ERROR START ----->
        <div class="modal fade" id="errorModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <asp:UpdatePanel ID="UpdatePanelError" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-content panel-danger">
                            <div class="modal-header">
                                <span style="padding:10px; margin-left:-10px; margin-right:15px;"><asp:ImageButton ID="ImageButton3" CssClass="close" Width="50" Height="50" ImageUrl="~/Public/Images/cancel.png" data-dismiss="modal" aria-hidden="true" runat="server" /></span>
                                <h4 class="modal-title"><asp:Label ID="Label3" runat="server" Text="Error"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <asp:Label ID="labelError" runat="server" ></asp:Label>
                                <br />
                                <div  id="divTable" runat="server" style="width:100%;"></div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Aceptar</button>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <!----- MODAL ERROR END ----->

</asp:Content>


