﻿<%@ page title="" language="C#" masterpagefile="~/Layout.master" autoeventwireup="true" inherits="Clientes_Doctores, App_Web_ea2af4wq" %>


<asp:Content ID="Content_Principal" ContentPlaceHolderID="ContentPlaceHolder_Principal" runat="Server">

    <script type="text/javascript">

        //        function ASPxUploadControl_FotoNueva_OnFileUploadComplete(s, e) {
        //            if (e.isValid)
        //                document.getElementById("uploadedImage").src = "Documentos/temp/" + e.callbackData;
        //            setElementVisible("uploadedImage", e.isValid);
        //        }

    </script>

    <!-- UPDATEPROGRESS START -->
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div class="image-loading-container">
                <asp:Image ID="imgUpdateProgress" runat="server" CssClass="image-loading" ImageUrl="~/Public/Images/load.gif" AlternateText="Loading ..." ToolTip="Loading ..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <!-- UPDATEPROGRESS END -->

    <!-- CONTAINER START -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">

                <!-- NAV START -->
                <ul class="nav u-nav-v5-1" role="tablist" data-target="nav-5-1-default-hor-left-icons" data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-lightgray">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#TabBusquedaDoctores" role="tab" onclick="CambiarTab_BuscarDoctores()">
                            <i class="et-icon-clipboard u-tab-line-icon-pro g-mr-3"></i>
                            Búsqueda de doctores
                        </a>
                    </li>
                </ul>
                <!-- NAV END -->

                <!-- TAB PANES START -->
                <div id="nav-5-1-default-hor-left-icons" class="tab-content g-pt-20">

                    <!-- PESTAÑA BUSQUEDA DE CLIENTES START -->
                    <div class="tab-pane fade show active" id="TabBusquedaDoctores" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanel_TabBusquedaDoctores" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Búsqueda de doctores</h3>
                                    </header>
                                    <section class="card-block">

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label>Seleccione un doctor para mostrar</label>
                                                    <dx:ASPxGridLookup ID="ASPxGridLookup_TabBusquedaDoctores_DoctoresActivos" AutoPostBack="true" OnDataBinding="ASPxGridLookup_TabBusquedaDoctores_DoctoresActivos_OnDataBinding" OnValueChanged="ASPxGridLookup_TabBusquedaDoctores_DoctoresActivos_OnValueChanged" Theme="iOS" runat="server" placeholder="Seleccione un doctor" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{1}" Width="100%" ClientInstanceName="ClientGridLookup">
                                                        <ClearButton DisplayMode="OnHover"></ClearButton>
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Width="30%" />
                                                            <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE" Width="30%" />
                                                            <dx:GridViewDataTextColumn Caption="Correo electrónico" FieldName="CORREO" Width="30%" />
                                                            <dx:GridViewDataTextColumn Caption="Número telefónico" FieldName="NUMERO" Width="30%" />
                                                        </Columns>
                                                        <GridViewStyles>
                                                            <AlternatingRow Enabled="true" />
                                                            <Cell Wrap="True" Font-Size="Small"></Cell>
                                                            <Header Wrap="True" Font-Size="Small"></Header>
                                                        </GridViewStyles>
                                                        <GridViewProperties>
                                                            <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                            <SettingsPager NumericButtonCount="3" />
                                                        </GridViewProperties>
                                                    </dx:ASPxGridLookup>
                                                </div>
                                                <div class="col-sm-12 col-md-2">
                                                    <asp:Button ID="Button_TabBusquedaDoctores_BuscarDoctor" OnClick="Button_TabBusquedaDoctores_BuscarDoctor_OnClick" runat="server" CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15" Style="display: inline-block; width: 100%; margin-top: 25px;" Text="Buscar" />
                                                </div>
                                                <div class="col-sm-12 col-md-2">
                                                    <asp:Button ID="Button_TabBusquedaDoctores_NuevoDoctor" OnClick="Button_TabBusquedaDoctores_NuevoDoctor_OnClick" runat="server" CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15" Style="display: inline-block; width: 100%; margin-top: 25px;" Text="Nuevo Doctor" />
                                                </div>
                                                <div class="col-sm-12 col-md-2">
                                                    <asp:Button ID="Button_TabBusquedaDoctores_ConvertirDoctor" OnClick="Button_TabBusquedaDoctores_ConvertirDoctor_Click" Enabled="false" runat="server" CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15" Style="display: inline-block; width: 100%; margin-top: 25px;" Text="Convertir a Doctor" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3 g-mb-30 g-mb-0--md">
                                                <div class="h-100 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md">
                                                    <section class="text-center g-mb-30 g-mb-50--md">
                                                        <div class="d-inline-block g-pos-rel g-mb-20">
                                                            <%--<a class="u-badge-v2--lg u-badge--bottom-right g-width-32 g-height-32 g-bg-lightblue-v3 g-bg-primary--hover g-mb-20 g-mr-20" href="#" onclick="CambiarImagenPerfil()">
                                                                <i class="hs-admin-pencil g-absolute-centered g-font-size-16 g-color-white"></i>
                                                            </a>--%>
                                                            <asp:Image ID="Image_TabBusquedaDoctores_FotoPerfil" runat="server" CssClass="img-fluid rounded-circle" AlternateText="Foto de perfil" ImageUrl="~/Public/Images/noavatar.jpg" />
                                                        </div>
                                                        <h5 id="Label_TabBusquedaDoctores_Cliente" runat="server" visible="false"></h5>
                                                        <h5 id="Label_TabBusquedaDoctores_FechaAlta" runat="server" class="g-font-weight-300 g-font-size-15 g-color-black mb-0">Fecha de alta: </h5>
                                                        <h5 id="Label_TabBusquedaDoctores_UsuarioAlta" runat="server" class="g-font-weight-300 g-font-size-15 g-color-black mb-0">Usuario alta: </h5>

                                                        <asp:HiddenField ID="HiddenField_IdDoctor" runat="server"></asp:HiddenField>
                                                        <%--<dx:ASPxUploadControl ID="ASPxUploadControl_FotoNueva" OnFileUploadComplete="ASPxUploadControl_FotoNueva_OnFileUploadComplete" Theme="Moderno" ClientInstanceName="UploadControl" FileUploadMode="OnPageLoad" runat="server" UploadMode="Auto" AutoStartUpload="true" Width="100%" ShowProgressPanel="True" CssClass="file" DialogTriggerID="externalDropZone">
                                                            <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="False" ExternalDropZoneID="externalDropZone" DropZoneText="" />
                                                            <ValidationSettings MaxFileSize="4194304" AllowedFileExtensions=".jpg, .jpeg, .gif, .png" ErrorStyle-CssClass="validationMessage" />
                                                            <BrowseButton Text="Buscar imagen..."  />
                                                            <DropZoneStyle CssClass="uploadControlDropZone" />
                                                            <ProgressBarStyle CssClass="uploadControlProgressBar" />
                                                            <ClientSideEvents 
                                                                DropZoneEnter="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', true); }" 
                                                                DropZoneLeave="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', false); }" 
                                                                FileUploadComplete="ASPxUploadControl_FotoNueva_OnFileUploadComplete">
                                                            </ClientSideEvents>
                                                        </dx:ASPxUploadControl>--%>
                                                    </section>
                                                    <!-- SIDEBAR START -->
                                                    <section>
                                                        <ul class="list-unstyled mb-0">
                                                            <li class="g-brd-top g-brd-gray-light-v7 mb-0">
                                                                <asp:LinkButton ID="Link_TabBusquedaDoctores_InformacionGeneral" runat="server" OnClick="Link_TabBusquedaDoctores_InformacionGeneral_OnClick" class="d-flex align-items-center u-link-v5 g-parent g-py-15 active">
                                                                    <span class="g-font-size-18 g-color-gray-light-v6 g-color-lightred-v3--parent-hover g-color-lightred-v3--parent-active g-mr-15">
						                                                <i class="hs-admin-user"></i>
					                                                </span>
                                                                    <span class="g-color-gray-dark-v6 g-color-lightred-v3--parent-hover g-color-lightred-v3--parent-active">Información general</span>
                                                                </asp:LinkButton>
                                                            </li>
                                                        </ul>
                                                    </section>
                                                    <!-- SIDEBAR END -->
                                                </div>
                                            </div>

                                            <div class="col-md-9">

                                                <!-- PANEL INFORMACION GENERAL START -->
                                                <asp:Panel ID="Panel_TabBusquedaDoctores_InformacionGeneral" runat="server">
                                                    <div class="h-100 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md">
                                                        <header>
                                                            <h2 class="text-uppercase g-font-size-12 g-font-size-default--md g-color-black mb-0">Información general</h2>
                                                        </header>
                                                        <hr class="d-flex g-brd-gray-light-v7 g-my-15 g-my-30--md">
                                                        <div class="row g-mb-20">
                                                            <div class="col-md-3 align-self-center g-mb-5 g-mb-0--md">
                                                                <label class="mb-0" for="#firstName">Nombre</label>
                                                            </div>
                                                            <div class="col-md-9 align-self-center">
                                                                <div class="form-group g-pos-rel mb-0">
                                                                    <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
                                                                        <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
                                                                    </span>
                                                                    <asp:TextBox ID="TextBox_TabBusquedaDoctores_Nombre" runat="server" CssClass="form-control form-control-md g-brd-gray-light-v7 g-brd-lightblue-v3--focus g-brd-lightred-v2--error g-rounded-4 g-px-20 g-py-12" PlaceHolder="Nombre"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row g-mb-20">
                                                            <div class="col-md-3 align-self-center g-mb-5 g-mb-0--md">
                                                                <label class="mb-0" for="#lastName">Correo electrónico</label>
                                                            </div>
                                                            <div class="col-md-9 align-self-center">
                                                                <div class="form-group g-pos-rel mb-0">
                                                                    <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
                                                                        <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
                                                                    </span>
                                                                    <asp:TextBox ID="TextBox_TabBusquedaDoctores_CorreoElectronico" runat="server" CssClass="form-control form-control-md g-brd-gray-light-v7 g-brd-lightblue-v3--focus g-brd-lightred-v2--error g-rounded-4 g-px-20 g-py-12" PlaceHolder="Correo electrónico"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row g-mb-20">
                                                            <div class="col-md-3 align-self-center g-mb-5 g-mb-0--md">
                                                                <label class="mb-0" for="#lastName">Número telefónico</label>
                                                            </div>
                                                            <div class="col-md-9 align-self-center">
                                                                <div class="form-group g-pos-rel mb-0">
                                                                    <span class="g-pos-abs g-top-0 g-right-0 d-block g-width-40 h-100 opacity-0 g-opacity-1--success">
                                                                        <i class="hs-admin-check g-absolute-centered g-font-size-default g-color-lightblue-v3"></i>
                                                                    </span>
                                                                    <asp:TextBox ID="TextBox_TabBusquedaDoctores_NumeroTelefonico" runat="server" CssClass="form-control form-control-md g-brd-gray-light-v7 g-brd-lightblue-v3--focus g-brd-lightred-v2--error g-rounded-4 g-px-20 g-py-12" PlaceHolder="Número telefónico"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="text-right" style="margin-top: 200px;">
                                                            <asp:Button ID="Button_TabBusquedaDoctores_GuardarCambios" OnClick="Button_TabBusquedaDoctores_GuardarCambios_OnClick" runat="server" CssClass="btn btn-md btn-xl--md u-btn-lightblue-v3 g-width-160--md g-font-size-12 g-font-size-default--md g-mb-10" Text="Guardar cambios" />
                                                            <asp:Button ID="Button_TabBusquedaDoctores_EliminarDoctor" OnClick="Button_TabBusquedaDoctores_EliminarDoctor_OnClick" runat="server" CssClass="btn btn-md btn-xl--md u-btn-red g-width-160--md g-font-size-12 g-font-size-default--md g-mb-10" Text="Eliminar doctor" />
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <!-- PANEL INFORMACION GENERAL END -->

                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!-- PESTAÑA BUSQUEDA DE CLIENTES END -->

                </div>
                <!-- TAB PANES END -->

            </div>
        </div>
    </div>
    <!-- CONTAINER END -->

    <!-- MODAL EDITAR DOCTOR START -->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0 dxpc-content-wi" Width="700px" ID="Modal_NuevoDoctor" ClientInstanceName="Modal_NuevoDoctor" runat="server" ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True" PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_Modal_NuevoDoctor" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                            <i class="fa fa-tasks g-font-size-default g-mr-5"></i>
                            Nuevo doctor
                        </h3>
                        <div class="card-block">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <label>Nombre del doctor</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoDoctor_NombreDoctor" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="Nombre del doctor"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoDoctor_NombreDoctor" runat="server" TargetControlID="TextBox_Modal_NuevoDoctor_NombreDoctor" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label>Correo electrónico</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoDoctor_CorreoElectronico" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="Correo electrónico"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoDoctor_CorreoElectronico" runat="server" TargetControlID="TextBox_Modal_NuevoDoctor_CorreoElectronico" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <label>Número telefónico</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoDoctor_NumeroTelefonico" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="Número telefónico"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoDoctor_NumeroTelefonico" runat="server" TargetControlID="TextBox_Modal_NuevoDoctor_NumeroTelefonico" FilterMode="InvalidChars" InvalidChars="'" />
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtenderNumeric_TextBox_Modal_NuevoDoctor_NumeroTelefonico" runat="server" TargetControlID="TextBox_Modal_NuevoDoctor_NumeroTelefonico" FilterType="Custom, Numbers" ValidChars="." />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-right">
                                        <asp:Button ID="Button_Modal_NuevoDoctor_GuardarDoctor" OnClick="Button_Modal_NuevoDoctor_GuardarDoctor_OnClick" runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15" Text="Guardar Cambios" />
                                        <asp:Button ID="Button_Modal_NuevoDoctor_CancelarNuevoDoctor" OnClick="Button_Modal_NuevoDoctor_CancelarNuevoDoctor_OnClick" runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15" Text="Cancelar" />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!-- MODAL EDITAR DOCTOR END -->

     <!-- MODAL EDITAR USUARIO START -->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0 dxpc-content-wi" Width="700px" ID="Modal_NuevoCliente" ClientInstanceName="Modal_NuevoCliente" runat="server" ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True" PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_Modal_NuevoCliente" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                            <i class="fa fa-tasks g-font-size-default g-mr-5"></i>
                            Nuevo cliente
                        </h3>
                        <div class="card-block">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <label>RFC</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoCliente_RFC" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="RFC"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoCliente_RFC" runat="server" TargetControlID="TextBox_Modal_NuevoCliente_RFC" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <label>Calle</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoCliente_Calle" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="Calle"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoCliente_Calle" runat="server" TargetControlID="TextBox_Modal_NuevoCliente_Calle" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label>Número Interior</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoCliente_Numero_Interior" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="Número Interior"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoCliente_Numero_Interior" runat="server" TargetControlID="TextBox_Modal_NuevoCliente_Numero_Interior" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <label>Número Exterior</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoCliente_Numero_Exterior" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="Número Exterior"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoCliente_Numero_Exterior" runat="server" TargetControlID="TextBox_Modal_NuevoCliente_Numero_Exterior" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-8">
                                        <label>Colonia</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoCliente_Colonia" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="Colonia"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoCliente_Colonia" runat="server" TargetControlID="TextBox_Modal_NuevoCliente_Colonia" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label>Código postal</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoCliente_Codigo" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="Código Postal"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoCliente_Codigo" runat="server" TargetControlID="TextBox_Modal_NuevoCliente_Codigo" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>

                             <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <label>Municipio</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoCliente_Municipio" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="Municipio"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoCliente_Municipio" runat="server" TargetControlID="TextBox_Modal_NuevoCliente_Municipio" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label>Estado</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoCliente_Estado" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="Estado"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoCliente_Estado" runat="server" TargetControlID="TextBox_Modal_NuevoCliente_Estado" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label>País</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoCliente_Pais" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="País"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoCliente_Pais" runat="server" TargetControlID="TextBox_Modal_NuevoCliente_Pais" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>
                            

                            <div class="form-group">
                                <div class="row">
                                     <div class="col-sm-12 col-md-12">
                                        <label>Dias de crédito</label>
                                        <asp:TextBox ID="TextBox_Modal_NuevoCliente_DiasCredito" Enabled="true" Visible="true" runat="server" CssClass="form-control rounded-0" PlaceHolder="Días de Crédito"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_NuevoCliente_DiasCredito" runat="server" TargetControlID="TextBox_Modal_NuevoCliente_DiasCredito" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" >
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-right">
                                        <asp:Button ID="Button_Modal_NuevoCliente_GuardarCliente" OnClick="Button_Modal_NuevoCliente_GuardarCliente_OnClick" runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15" Text="Guardar Cambios" />
                                        <asp:Button ID="Button_Modal_NuevoCliente_CancelarNuevoCliente" OnClick="Button_Modal_NuevoCliente_CancelarNuevoCliente_OnClick" runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15" Text="Cancelar" />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!-- MODAL EDITAR USUARIO END -->
</asp:Content>


