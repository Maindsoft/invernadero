﻿<%@ page title="Gestión de usuarios" language="C#" masterpagefile="~/Layout.master" autoeventwireup="true" inherits="Usuarios, App_Web_dbcrxqvf" %>

<asp:Content ID="Content_Principal" ContentPlaceHolderID="ContentPlaceHolder_Principal" runat="Server">

    <script type="text/javascript">

        function CambiarTabCapturaUsuarios() {
            document.getElementById('<%=LinkButton_TabCapturaUsuarios_Actualizar.ClientID%>').click();
        }


    </script>

    <!-- UPDATEPROGRESS START -->
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div class="image-loading-container">
                <asp:Image ID="imgUpdateProgress" runat="server" CssClass="image-loading" ImageUrl="~/Public/Images/load.gif" AlternateText="Loading ..." ToolTip="Loading ..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <!-- UPDATEPROGRESS END -->

    <!-- CONTAINER START -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">

                <!-- NAV START -->
                <ul class="nav u-nav-v5-1" role="tablist" data-target="nav-5-1-default-hor-left-icons" data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-lightgray">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#TabCapturaUsuarios" role="tab" onclick="CambiarTabCapturaUsuarios()">
                            <i class="et-icon-clipboard u-tab-line-icon-pro g-mr-3"></i>
                            Captura de usuarios
                        </a>
                    </li>
                </ul>
                <!-- NAV END -->

                <!-- TAB PANES START -->
                <div id="nav-5-1-default-hor-left-icons" class="tab-content g-pt-20">

                    <!-- PESTAÑA CAPTURA DE USUARIOS START -->
                    <div class="tab-pane fade show active" id="TabCapturaUsuarios" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanel_TabCapturaUsuarios" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Captura de Usuarios</h3>
                                        <asp:LinkButton ID="LinkButton_TabCapturaUsuarios_DescargarExcel" OnClick="LinkButton_TabCapturaUsuarios_DescargarExcel_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-bluegray g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Descargar Excel</span>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton_TabCapturaUsuarios_Actualizar" OnClick="LinkButton_TabCapturaUsuarios_Actualizar_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-teal g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Actualizar</span>
                                        </asp:LinkButton>
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-6">
                                                        <label>Nombre</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaUsuarios_Nombre" runat="server" CssClass="form-control" PlaceHolder="Nombre del usuario"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaUsuarios_Nombre" runat="server" TargetControlID="TextBox_TabCapturaUsuarios_Nombre" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Apellido paterno</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaUsuarios_ApellidoPaterno" runat="server" CssClass="form-control" PlaceHolder="Apellido paterno"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaUsuarios_ApellidoPaterno" runat="server" TargetControlID="TextBox_TabCapturaUsuarios_ApellidoPaterno" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Apellido materno</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaUsuarios_ApellidoMaterno" runat="server" CssClass="form-control" PlaceHolder="Apellido materno"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaUsuarios_ApellidoMaterno" runat="server" TargetControlID="TextBox_TabCapturaUsuarios_ApellidoMaterno" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-6">
                                                        <label>Perfil de puesto</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto" OnDataBinding="ASPxGridLookup_TabCapturaUsuarios_PerfilesPuesto_OnDataBinding" Theme="iOS" runat="server" placeholder="Perfiles de puesto" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{1}" Width="100%" ClientInstanceName="ClientGridLookup">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Width="30%" />
                                                                <dx:GridViewDataTextColumn Caption="Descripción" FieldName="DESCRIPCION" Width="30%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                    <div class="col-sm-12 col-md-6">
                                                        <label>Correo electrónico</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaUsuarios_CorreoElectronico" runat="server" CssClass="form-control" PlaceHolder="Correo electrónico"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaUsuarios_CorreoElectronico" runat="server" TargetControlID="TextBox_TabCapturaUsuarios_CorreoElectronico" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">

                                                <div class="col-sm-12 col-md-6">
                                                    <label>Contraseña</label>
                                                    <asp:TextBox ID="TextBox_TabCapturaUsuarios_Password" runat="server" CssClass="form-control" TextMode="Password" PlaceHolder="Contraseña"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaUsuarios_Password" runat="server" TargetControlID="TextBox_TabCapturaUsuarios_Password" FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>

                                                <div class="col-sm-12 col-md-6 text-right">
                                                    <asp:Button ID="Button1" OnClick="Button_TabCapturaUsuarios_Guardar_OnClick" runat="server" CssClass="btn btn-info" Text="Guardar Usuario" Style="display: inline-block; margin-top: 20px;" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <dx:ASPxGridView ID="ASPxGridView_TabCapturaUsuarios_UsuariosActivos" OnRowCommand="ASPxGridView_TabCapturaUsuarios_UsuariosActivos_OnRowCommand" OnDataBinding="ASPxGridView_TabCapturaUsuarios_UsuariosActivos_OnDataBinding" Theme="Material" runat="server" AutoGenerateColumns="False" KeyFieldName="ID" Width="100%">
                                                        <Columns>

                                                            <dx:GridViewDataColumn Caption="Editar" Name="Buttons" Width="100">
                                                                <EditFormSettings Visible="False" />
                                                                <DataItemTemplate>
                                                                    <center>
                                                                            <asp:LinkButton ID="ASPxButton2" runat="server" CommandName="EDITAR" ToolTip="Editar número de parte">
                                                                                <i class="fa fa-edit fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="Eliminar" Name="Buttons" Width="100">
                                                                <EditFormSettings Visible="False" />
                                                                <DataItemTemplate>
                                                                    <center>
                                                                            <asp:LinkButton ID="ASPxButton20" runat="server" CommandName="ELIMINAR" ToolTip="Eliminar número de parte">
                                                                                <i class="fa fa-times-rectangle-o fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>

                                                            <dx:GridViewDataTextColumn FieldName="ID" Caption="ID" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                                <CellStyle HorizontalAlign="Center"></CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Nombre" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                            <dx:GridViewDataTextColumn FieldName="CORREO_ELECTRONICO" Caption="Correo electrónico" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="NOMBRE_PERFIL" Caption="Perfil de puesto" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />



                                                        </Columns>
                                                        <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Auto" VerticalScrollableHeight="400" ShowFooter="true" />
                                                        <Settings ShowGroupPanel="true" />
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                        <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                        <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                        <Styles>
                                                            <AlternatingRow Enabled="true" />
                                                            <Cell Wrap="True" Font-Size="Small" />
                                                            <Header Wrap="True" Font-Size="Small" />
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter_ASPxGridView_TabCapturaUsuarios_UsuariosActivos" runat="server" GridViewID="ASPxGridView_TabCapturaUsuarios_UsuariosActivos" />
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                </section>
                                </div>
                           
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="LinkButton_TabCapturaUsuarios_DescargarExcel" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <!-- PESTAÑA CAPTURA DE USUARIOS END -->

                </div>
                <!-- TAB PANES END -->

            </div>
        </div>
    </div>
    <!-- CONTAINER END -->


    <!-- MODAL EDITAR USUARIO START -->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0 dxpc-content-wi" Width="900px" ID="Modal_EditarUsuario" ClientInstanceName="Modal_EditarUsuario" runat="server" ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True" PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_ModalEditarUsuario" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <!-- Info Outline Panel-->
                        <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                            <i class="fa fa-tasks g-font-size-default g-mr-5"></i>
                            Editar Usuario
                            </h3>
                        <div class="card-block">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label>Nombre</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarUsuario_Nombre" runat="server" CssClass="form-control" PlaceHolder="Nombre del usuario"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarUsuario_Nombre" runat="server" TargetControlID="TextBox_Modal_EditarUsuario_Nombre" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Apellido paterno</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarUsuario_ApellidoPaterno" runat="server" CssClass="form-control" PlaceHolder="Apellido paterno"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarUsuario_ApellidoPaterno" runat="server" TargetControlID="TextBox_Modal_EditarUsuario_ApellidoPaterno" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Apellido materno</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarUsuario_ApellidoMaterno" runat="server" CssClass="form-control" PlaceHolder="Apellido materno"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarUsuario_ApellidoMaterno" runat="server" TargetControlID="TextBox_Modal_EditarUsuario_ApellidoMaterno" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label>Perfil de puesto</label>
                                        <dx:ASPxGridLookup ID="ASPxGridLookup_Modal_EditarUsuario_PerfilPuesto" OnDataBinding="ASPxGridLookup_Modal_EditarUsuario_PerfilPuesto_OnDataBinding" Theme="iOS" runat="server" placeholder="Perfiles de puesto" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{1}" Width="100%" ClientInstanceName="ClientGridLookup">
                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Width="30%" />
                                                <dx:GridViewDataTextColumn Caption="Descripción" FieldName="DESCRIPCION" Width="30%" />
                                            </Columns>
                                            <GridViewStyles>
                                                <AlternatingRow Enabled="true" />
                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                <Header Wrap="True" Font-Size="Small"></Header>
                                            </GridViewStyles>
                                            <GridViewProperties>
                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                <SettingsPager NumericButtonCount="3" />
                                            </GridViewProperties>
                                        </dx:ASPxGridLookup>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label>Correo electrónico</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarUsuario_CorreoElectronico" runat="server" CssClass="form-control" PlaceHolder="Correo electrónico"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarUsuario_CorreoElectronico" runat="server" TargetControlID="TextBox_Modal_EditarUsuario_CorreoElectronico" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <label>Contraseña</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarUsuario_Password" runat="server" CssClass="form-control" TextMode="Password" PlaceHolder="Contraseña"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarUsuario_Password" runat="server" TargetControlID="TextBox_Modal_EditarUsuario_Password" FilterMode="InvalidChars" InvalidChars="'" />
                                        <small>NOTA: Si llena el campo contraseña se reemplazará por la contraseña actual (esta acción no puede cambiarse)</small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top: 100px;">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-right">
                                        <asp:Button ID="Button_ModalEditarUsuario_GuardarCambios" OnClick="Button_ModalEditarUsuario_GuardarCambios_OnClick" runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15" Text="Guardar Cambios" />
                                        <asp:Button ID="Button_ModalEditarUsuario_CerrarModal" OnClick="Button_ModalEditarUsuario_CerrarModal_OnClick" runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15" Text="Cancelar" />
                                    </div>
                                </div>
                            </div>
                            <!-- CAMPOS OCULTOS START -->
                            <asp:TextBox ID="TextBox_ModalEditarUsuario_IdUsuario" runat="server" CssClass="hidden-control"></asp:TextBox>
                            <asp:HiddenField ID="HiddenField_ModalEditarUsuario_IdUsuario" runat="server" />
                            <!-- CAMPOS OCULTOS END -->
                        </div>
                        <!-- End Info Outline Panel-->
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!-- MODAL EDITAR USUARIO END -->

</asp:Content>

