﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilidades
{
    public class Limpiador
    {
        public static string LimpiarComillaSimple(string Entrada)
        {
            return Entrada.Replace('\'', '"');
        }

        public static string LimpiarEspaciosEnBlanco(string Entrada)
        {
            return Entrada.Trim();
        }
    }
}
