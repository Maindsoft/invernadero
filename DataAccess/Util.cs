﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Web;

namespace DataAccess
{
    public class Util
    {
        /// <summary>
        /// Método que retorna el nombre de la cadena de conexión actual
        /// </summary>
        /// <returns></returns>
        public static string SRVMICSAConnectionString()
        {
            string strConnection;

            ///CONEXION AL SERVIDOR DE MAINDSOFT PARA PRODUCCION 
            strConnection = "SRV_MAINDSOFT__INVERNADERO";

            ///CONEXION AL SERVIDOR DE MAINDSOFT PARA HACER PRUEBAS
              //strConnection = "SRV_MAINDSOFT__INVERNADERO_PRUEBAS";


            return strConnection;
        }

        /// <summary>
        /// Método que retorna el nombre del servidor perteneciente a la cadena de conexión actual
        /// </summary>
        /// <returns></returns>
        public static string getServerName()
        {
            string serverName = "";
            //Nombre del servidor
            switch (SRVMICSAConnectionString().Trim())
            {
                case "SRV_MAINDSOFT__INVERNADERO":
                case "SRV_MAINDSOFT__INVERNADERO_PRUEBAS":
                    serverName = "107.180.90.5";
                    break;

            }
            return serverName;
        }

        /// <summary>
        /// Método que retorna el nombre del perfil de la base de datos que pertenece a la cadena de conexión actual
        /// </summary>
        /// <returns></returns>
        public static string getProfileName()
        {
            string profile = "";
            switch (SRVMICSAConnectionString().Trim())
            {

                case "SRV_MAINDSOFT__INVERNADERO":
                case "SRV_MAINDSOFT__INVERNADERO_PRUEBAS":
                    //profile = "MAINDSOFT";
                    profile = "INVERNADERO";
                    break;
            }
            return profile;
        }

        /// <summary>
        /// Método que retorna el nombre de la base de datos que pertenece a la cadena de conexión actual
        /// </summary>
        /// <returns></returns>
        public static string getNombreBaseDatos()
        {
            string bd = "";
            //Nombre del servidor
            switch (SRVMICSAConnectionString().Trim())
            {

                case "SRV_MAINDSOFT__INVERNADERO":
                    bd = "INVERNADERO";
                    break;

                case "SRV_MAINDSOFT__INVERNADERO_PRUEBAS":
                    bd = "INVERNADERO_STAGE";
                    break;
            }
            return bd;
        }

        /// <summary>
        /// Método que retorna el nombre del usuario de la base de datos que pertenece a la cadena de conexión actual
        /// </summary>
        /// <returns></returns>
        public static string getUsuario()
        {
            string userName = "";
            switch (SRVMICSAConnectionString().Trim())
            {
                case "SRV_MAINDSOFT__INVERNADERO":
                case "SRV_MAINDSOFT__INVERNADERO_PRUEBAS":
                    userName = "publico";
                    break;
            }
            return userName;
        }

        /// <summary>
        /// Método que retorna la contraseña de la base de datos que pertenece a la cadena de conexión actual
        /// </summary>
        /// <returns></returns>
        public static string getcontraseña()
        {
            string pass = "";
            switch (SRVMICSAConnectionString().Trim())
            {
                case "SRV_MAINDSOFT__INVERNADERO":
                case "SRV_MAINDSOFT__INVERNADERO_PRUEBAS":
                    pass = "3087890299";
                    break;
            }
            return pass;
        }

        /// <summary>
        /// Método que retorna la opción de seguridad integrada que pertenece a la cadena de conexión actual
        /// </summary>
        /// <returns></returns>
        public static bool getIntegratedSecurity()
        {
            bool security = false;
            switch (SRVMICSAConnectionString().Trim())
            {
                case "SRV_MAINDSOFT__INVERNADERO":
                case "SRV_MAINDSOFT__INVERNADERO_PRUEBAS":
                    security = false;
                    break;
            }
            return security;
        }

        public static bool esTimbradoDePrueba()
        {
            bool dePruebas = true;

            switch (SRVMICSAConnectionString().Trim())
            {
                //---------------->WEB SERVICE DE PRODUCCION<-------------------
                //---->SERVIDOR EN LA NUBE<--------------                
                //case "NISSAN":
                case "SRVMAINDSOFT_MULETTA_ERP":
                case "SRVMAINDSOFT_MELO_ERP":
                case "SRVMAINDSOFT_DMU":
                case "SRVMAINDSOFT_IASA":
                case "SRVMAINDSOFT_MAINDSOFT":
                case "SRVMAINDSOFT_SOLINDA":
                case "SRVMAINDSOFT_SOIN3":
                case "SRVMAINDSOFT_CIDEM":
                case "SRVMAINDSOFT_AYRVEN":
                case "SRVMAINDSOFT_SEDACEI":
                case "SRVMAINDSOFT_PLASMOL":
                case "SRVMAINDSOFT_MEQUIMSA":
                case "SRVMAINDSOFT_MAINDSOFT_DEMO":
                case "SRVMAINDSTEEL_MAINDSTEEL":
                case "SRVMAINDSTEEL_PREMADI":
                case "SRVMAINDSOFT_CARLSJR":
                case "SRVMAINDSTEEL_MAINDGREEN":
                case "SRVMAINDSOFT_COMPO":
                case "SRVMAINDSOFT_COMPO_MEXICO":


                //--->CONEXIONES DE [CVNS]<---
                //////-->SERIVIDOR EN CVNS, CONEXION REMOTA (IP PUBLICA)  Y LOCAL
                case "CVNS":
                case "CVNS_IP_PUBLICA":

                case "SRV_MAINDSOFT__INVERNADERO_PRUEBAS":


                    dePruebas = true;
                    break;


                //----------------->WEB SERVICE DE PRUEBAS <---
                case "NISSAN":
                //---->SERVIDOR EN LA NUBE<--------------
                case "SRVMAINDSOFT_NISSAN":
                case "SRVMAINDSOFT_NISSAN_PRUEBAS":
                case "SRVMAINDSOFT_DMU_PRUEBAS":
                case "SRVMAINDSOFT_MULETTA_ERP_PRUEBAS":
                case "SRVMAINDSOFT_IASA_PRUEBAS":
                case "SRVMAINDSOFT_SEDACEI_PRUEBAS":
                case "SRVMAINDSOFT_SOLINDA_PRUEBAS":
                case "SRVMAINDSOFT_PLASMOL_PRUEBAS":
                case "SRVMAINDSOFT_MEQUIMSA_PRUEBAS":
                case "SRVMAINDSOFT_CENTRAL_GAS":
                case "SRVMAINDSOFT_SOIN3_PRUEBAS":
                case "SRVMAINDSOFT_COMPO_PRUEBAS":
                case "SRVMAINDSOFT_COMPO_MEXICO_PRUEBAS":

                //temporal mientras se actualiza la FIEL
                case "SRVMAINDSOFT_MICAMPO":


                //--->SERVIDOR "LOCAL" EN MAINDSTEEL PARA PRUEBAS [192.168.1.201]<---
                case "SRVMAINDSTEEL_SOIN3":
                case "SRVMAINDSTEEL_SEDACEI":
                case "SRVMAINDSTEEL_MAINDSTEEL_PRUEBAS":
                case "SRVMAINDSTEEL_MAINDSTEEL_DEMO":
                case "SRVMAINDSTEEL_CVNS":

                //CONEXION LOCAL (EN TU COMPUTADORA) EL USUARIO QUEDA VACIO
                case "localConn":

                case "SRV_MAINDSOFT__INVERNADERO":

                    dePruebas = false;
                    break;
            }


            return dePruebas;
        }

        
    }
}
