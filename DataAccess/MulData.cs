﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace DataAccess
{
    public class MulData
    {
        public void UpdateCondicion(String StrCondicion)
        {
            Database dbDoc = DatabaseFactory.CreateDatabase(Util.SRVMICSAConnectionString());
            DbCommand cmdWrapper = dbDoc.GetSqlStringCommand("SET DATEFORMAT DMY UPDATE " + StrCondicion + "");
            dbDoc.ExecuteNonQuery(cmdWrapper);
        }
        public void InsertCondicion(String StrCondicion)
        {
            Database dbDoc = DatabaseFactory.CreateDatabase(Util.SRVMICSAConnectionString());
            DbCommand cmdWrapper = dbDoc.GetSqlStringCommand("SET DATEFORMAT DMY INSERT into " + StrCondicion + "");
            dbDoc.ExecuteNonQuery(cmdWrapper);
        }
        public void DeleteCondicion(String StrCondicion)
        {
            Database dbDoc = DatabaseFactory.CreateDatabase(Util.SRVMICSAConnectionString());
            DbCommand cmdWrapper = dbDoc.GetSqlStringCommand("SET DATEFORMAT DMY DELETE " + StrCondicion + "");
            dbDoc.ExecuteNonQuery(cmdWrapper);
        }
        public void QueryLibre2(String StrCondicion)
        {
            Database dbDoc = DatabaseFactory.CreateDatabase(Util.SRVMICSAConnectionString());
            DbCommand cmdWrapper = dbDoc.GetSqlStringCommand("" + StrCondicion + "");
            dbDoc.ExecuteNonQuery(cmdWrapper);
        }
        public DataTable SelectCondicion(String StrCondicion)
        {
            Database dbDoc = DatabaseFactory.CreateDatabase(Util.SRVMICSAConnectionString());
            DbCommand cmdDoc = dbDoc.GetSqlStringCommand("SET DATEFORMAT DMY SELECT " + StrCondicion + "");
            DataTable dtDoc = dbDoc.ExecuteDataSet(cmdDoc).Tables[0];
            return dtDoc;
        }
        public DataTable QueryLibre(String StrCondicion)
        {
            Database dbDoc = DatabaseFactory.CreateDatabase(Util.SRVMICSAConnectionString());
            DbCommand cmdDoc = dbDoc.GetSqlStringCommand("" + StrCondicion + "");
            DataTable dtDoc = dbDoc.ExecuteDataSet(cmdDoc).Tables[0];
            return dtDoc;
        }
        public void EnviarCorreo(String StrCorreo, String StrSubject, String StrBody)
        {
            
            Database dbDoc = DatabaseFactory.CreateDatabase(Util.SRVMICSAConnectionString());
            DbCommand cmdWrapper = dbDoc.GetSqlStringCommand("EXEC msdb.dbo.sp_send_dbmail @recipients='" + StrBody + "', "
                                                          + "@subject = '" + StrSubject + "',"
                                                          + "@body = '" + StrBody + "';");
            dbDoc.ExecuteNonQuery(cmdWrapper);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="StrCorreo"></param>
        /// <param name="StrSubject"></param>
        /// <param name="StrBody"></param>
        /// <param name="idModulo">Ingresa el id del módulo para agregar la liga al final del correo (OPCIONAL)</param>
        public void EnviarCorreoHTML(String StrCorreo, String StrSubject, String StrBody, int? idModulo = 0)
        {
            string liga = "";
            if (idModulo != 0)
            {
                DataTable ruta = SelectCondicion("(SELECT FE.DOMINIO FROM FACTURACION_EMISOR FE) + " +
                                                "(SELECT M.RUTA FROM MODULOS M WHERE ID_MODULO = " + idModulo + ") AS RUTA");
                DataRow rowRuta = ruta.Rows[0];
                liga = " <br/><br/><p>Entra <a href=\"" + rowRuta["RUTA"].ToString().Replace("~", "") + "\">aquí</a> para mas detalles.</p><br/><br/>";
            }
            else
            {
                liga = "";
            }
            DataTable dominios = SelectCondicion("DOMINIO FROM FACTURACION_EMISOR");
            DataRow rowDominios = dominios.Rows[0];
            string dominio = rowDominios["DOMINIO"].ToString();
            string StrHeader = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
                "	<head>" +
                "		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />" +
                "		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />" +
                "	</head>" +
                "	<body style=\"margin: 0; padding: 0;\">" +
                "		<table align=\"center\" bgcolor=\"#ebebeb\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"padding:0px; margin:0px;\" width=\"100%\">" +
                "			<tbody>" +
                "				<tr>" +
                "					<td>" +
                "						<table align=\"center\" bgcolor=\"#ebebeb\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">" +
                "							<tr>" +
                "								<td align=\"left\" height=\"20\" valign=\"top\">" +
                "									<img src=\"http://maindsteel-erp.com/Images/topshadowbasica.png\" style=\"display: block; width: 600px; height: 20px;\" />" +
                "								</td>" +
                "							</tr>" +
                "							<tr>" +
                "								<td align=\"right\" bgcolor=\"#d9d9d9\" style=\"padding: 0px 30px 0px 0px;\">" +
                "									<img src=\"" + dominio + "Images/logo.png\" style=\"display:block;\" width=\"140\" height=\"140\" />" +
                "								</td>" +
                "							</tr>" +
                "							<tr>" +
                "								<td align=\"left\" bgcolor=\"#ffffff\" height=\"15\" valign=\"top\">" +
                "									<img src=\"http://maindsteel-erp.com/Images/shadowbasica.png\" style=\"display: block; width: 600px; height: 15px;\" />" +
                "								</td>" +
                "							</tr>" +
                "							<tr>" +
                "								<td bgcolor=\"#ffffff\" style=\"padding: 0px 30px 40px 30px; font-family:Arial, Helvetica, sans-serif;\">" +
                StrBody + liga +
                "                               </td>" +
                "							</tr>" +
                "							<tr>" +
                "								<td align=\"left\" height=\"15\" valign=\"top\">" +
                "									<img src=\"http://maindsteel-erp.com/Images/shadowbasica.png\" style=\"display: block; width: 600px; height: 15px;\" />" +
                "								</td>" +
                "							</tr>" +
                "							<tr>" +
                "								<td align=\"left\" bgcolor=\"#ebebeb\" style=\"padding: 0px 30px 5px 30px; font-family:Arial, Helvetica, sans-serif; font-size:12px;\">" +
                "									MaindSoft ERP " +
                "									<a href=\"http://maindsoft.net/\" target=\"_blank\">" +
                "										<img src=\"http://maindsteel-erp.com/images/LogoMaindosoft.png\" align=\"right\" style=\"display:block;\" width=\"62\" height=\"32\" />"+
                "									</a>" +
                "								</td>" +
                "							</tr>" +
                "						</table>" +
                "					</td>" +
                "				</tr>" +
                "			</tbody>" +
                "		</table>" +
                "	</body>" +
                "</html>";
            Database dbDoc = DatabaseFactory.CreateDatabase(Util.SRVMICSAConnectionString());
            DbCommand cmdWrapper = dbDoc.GetSqlStringCommand("EXEC msdb.dbo.sp_send_dbmail "
                                                          + "@profile_name = '" + Util.getProfileName() + "',"
                                                          +" @recipients='" + StrCorreo + "', "
                                                          + "@subject = '" + StrSubject + "',"
                                                          + "@body = '" + StrHeader + "',"
                                                          + "@body_format = 'HTML' ;");
            dbDoc.ExecuteNonQuery(cmdWrapper);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="StrProfileName"></param>
        /// <param name="StrCorreo"></param>
        /// <param name="StrCorreoCopia"></param>
        /// <param name="StrSubject"></param>
        /// <param name="StrBody"></param>
        /// <param name="StrAttach"></param>
        /// <param name="idModulo">Ingresa el id del módulo para agregar la liga al final del correo (OPCIONAL)</param>
        public void EnviarCorreoHTMLAdjuntos(String StrProfileName, String StrCorreo, String StrCorreoCopia, String StrSubject, String StrBody, String StrAttach, int? idModulo = 0)
        {
            string liga = "";
            if (idModulo != 0)
            {
                DataTable ruta = SelectCondicion("(SELECT FE.DOMINIO FROM FACTURACION_EMISOR FE) + " +
                                                "(SELECT M.RUTA FROM MODULOS M WHERE ID_MODULO = " + idModulo + ") AS RUTA");
                DataRow rowRuta = ruta.Rows[0];
                liga = " <br/><br/><p>Entra <a href=\"" + rowRuta["RUTA"].ToString().Replace("~", "") + "\">aquí</a> para mas detalles.</p><br/><br/>";
            }
            StrProfileName = Util.getProfileName();
            Database dbDoc = DatabaseFactory.CreateDatabase(Util.SRVMICSAConnectionString());
            DbCommand cmdWrapper = dbDoc.GetSqlStringCommand("EXEC msdb.dbo.sp_send_dbmail "
                                                          + "@profile_name = '" + StrProfileName + "',"
                                                          + "@recipients='" + StrCorreo + "', "
                                                          + "@subject = '" + StrSubject + "',"
                                                          + "@body = '" + StrBody + liga + "',"
                                                          + "@body_format = 'HTML',"
                                                          + "@file_attachments = '" + StrAttach + "',"
                                                          + "@copy_recipients = '" + StrCorreoCopia + "' ;");
            dbDoc.ExecuteNonQuery(cmdWrapper);
        }

        public void InsertQueryLibre(String StrCondicion)
        {
            Database dbDoc = DatabaseFactory.CreateDatabase(Util.SRVMICSAConnectionString());
            DbCommand cmdWrapper = dbDoc.GetSqlStringCommand(StrCondicion);
            dbDoc.ExecuteNonQuery(cmdWrapper);
        }
    }
}
