﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Objetos
{
    public class Cliente
    {
        public int id_cliente;

        public string nombre;

        public string rfc;

        public string calle;

        public string numInt;

        public string numExt;

        public string colonia;

        public string codigo;

        public string municipio;

        public string estado;

        public string pais;

        public string email;

        public string telefonico;

        public string diasCredito;

        public DateTime fecha_alta;

        public string usuario_alta;

        public int idTipoCliente;

        public DateTime FechaNacimiento;

    }
}
