﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Exceptions;

namespace Core.Facturación
{
    public class GestionFacturacion
    {
        public static bool crearNotaRemision(string idEstudio, string idCliente, string fechaFactura, int idUsuarioActualiza, string anticipo, string concepto) {
            string query =
                "EXEC [dbo].[CREAR_NOTA_REMISION] " +
                "@idEstudio = " + idEstudio + ", " +
                "@idCliente = " + idCliente + ", " +
                "@fechaFactura = '" + fechaFactura + "', " +
                "@idUsuarioActualiza = '" + idUsuarioActualiza + "', " +
                "@anticipo = " + anticipo+","+
                "@concepto = " + concepto;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar nota de remisión", exc);
            }
        }

        public static bool actualizarNotaRemision(string idEstudio, string idCliente, int idUsuarioActualiza)
        {
            string query =
                "EXEC [dbo].[ACTUALIZAR_NOTA_REMISION] " +
                "@idEstudio = " + idEstudio + ", " +
                "@idCliente = " + idCliente + ", " +
                "@idUsuarioActualiza = '" + idUsuarioActualiza + "'";

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para actualizar nota de remisión", exc);
            }
        }

        public static bool eliminarNotaRemision(string idEstudio, int idUsuarioActualiza)
        {
            string query =
                "EXEC [dbo].[ELIMINAR_NOTA_REMISION] " +
                "@idEstudio = " + idEstudio + ", " +
                "@idUsuarioActualiza = '" + idUsuarioActualiza + "'";

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para eliminar nota de remisión", exc);
            }
        }

        public static int obtenerTipoDocumento(string idEstudio, int id_usuario_alta)
        {
            string query =
                "EXEC [dbo].[OBTENER_TIPO_DOCUMENTO_CPC] " +
                "@idEstudio = " + idEstudio + ", " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {

                return Convert.ToInt32(BLL.MulData.QueryLibre(query).Rows[0]["ID_TIPO_DOCUMENTO"]);
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para crear un nuevo estudio", exc);
            }
        }
    }
}
