﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Exceptions;
using Core.Objetos;
using System.Data;

namespace Core.Clientes
{
    public class GestionDoctores
    {
        public static bool Crear(string nombre, string correo, string telefono, int idUsuario)
        {
            string query =
                "EXEC [dbo].[CREAR_DOCTOR] " +
                "@nombre = '" + nombre + "' , " +
                "@correoElectronico = '" + correo + "' , " +
                "@numeroTelefonico = '" + telefono + "' , " +
                "@idUsuarioActualiza = " + idUsuario;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para crear un nuevo usuario", exc);
            }

        }

        public static Doctor Obtener(int id_doctor, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_DATOS_DOCTOR] " +
                "@id_doctor = " + id_doctor + " , " +
                "@idUsuarioActualiza = " + id_usuario_alta;


            DataTable data = BLL.MulData.QueryLibre(query);

            Doctor retorno = new Doctor();

            retorno.id_doctor = Convert.ToInt32(data.Rows[0]["ID_DOCTOR"]);
            retorno.nombre = Convert.ToString(data.Rows[0]["NOMBRE"]);
            retorno.email = Convert.ToString(data.Rows[0]["CORREO_ELECTRONICO"]);
            retorno.telefonico = Convert.ToString(data.Rows[0]["NUMERO_TELEFONICO"]);
            retorno.id_cliente = Convert.ToInt32(data.Rows[0]["ID_CLIENTE"]);
            retorno.fecha_alta = Convert.ToDateTime(data.Rows[0]["FECHA_ALTA"]);
            retorno.usuario_alta = Convert.ToString(data.Rows[0]["USUARIO_ALTA"]);

            return retorno;
        }

        public static DataTable ObtenerDoctoresActivos(int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_DOCTORES_ACTIVOS] " +
                "@idUsuarioActualiza = " + id_usuario_alta;


            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static bool eliminar(int id_cliente, int usuario_actualiza)
        {
            string query =
               "EXEC [dbo].[ELIMINAR_DOCTOR]" +
               "@id_doctor = " + id_cliente + " ," +
               "@idUsuarioActualiza = " + usuario_actualiza;
            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para eliminar el doctor seleccionado", exc);
            }
        }

        public static bool ModificarDoctor(string idDoctor ,string nombre, string correo, string telefono, int id_usuario_alta)
        {
            string query =
                 "EXEC [dbo].[ACTUALIZAR_DATOS_DOCTOR] " +
                 "@id_doctor = "+ idDoctor +", "+
                 "@nombre = '" + nombre + "' , " +
                 "@correoElectronico = '" + correo + "' , " +
                 "@numeroTelefonico = '" + telefono + "' , " +
                 "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para modificar un cliente", exc);
            }
        }

        public static bool ConvertirACliente(string id_doctor,string rfc, string calle, string numInt, string numExt, string colonia, string codigo, string municipio, string estado, string pais, string diasCredito, int id_usuario_alta)
        {
            string query =
                "EXEC [dbo].[CONVERTIR_DOCTOR_A_CLIENTE] " +
                "@id_doctor = " + id_doctor + " , " +
                "@rfc = '" + rfc + "' , " +
                "@calle = '" + calle + "' , " +
                "@numInt = '" + numInt + "' , " +
                "@numExt = '" + numExt + "' , " +
                "@colonia = '" + colonia + "' , " +
                "@codigo = '" + codigo + "' , " +
                "@municipio = '" + municipio + "' , " +
                "@estado = '" + estado + "' , " +
                "@pais = '" + pais + "' , " +
                "@diasCredito = '" + diasCredito + "' , " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para crear un nuevo cliente", exc);
            }
        }
    }
}
