﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Exceptions;
using Core.Objetos;
using System.Data;

namespace Core.Estudios
{
    public class GestionEstudios
    {
        public static DataTable Crear(string idTipoEstudio, int id_usuario_alta)
        {
            string query =
                "EXEC [dbo].[CREAR_ESTUDIO] " +
                "@idTipoEstudio = " + idTipoEstudio + ", " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {

                return BLL.MulData.QueryLibre(query);
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para crear un nuevo estudio", exc);
            }
        }

        public static bool InsertarEspecimenAEstudio(string idEspecimen, string precioVenta, string cantidad, string claveEstudio, int idUsuarioActualiza)
        {
            string query =
                "EXEC [dbo].[INSERTAR_ESPECIMEN_A_ESTUDIO] " +
                "@idEspecimen = " + idEspecimen + ", " +
                "@precioVenta = " + precioVenta + ", " +
                "@cantidad = " + cantidad + ", " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + idUsuarioActualiza;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar especimen a estudio", exc);
            }
        }

        public static DataTable ObtenerEspecimenesDeEstudio(string claveEstudio, int idUsuarioActualiza)
        {
            string query =
                "EXEC [dbo].[OBTENER_ESPECIMENES_DE_ESTUDIO] " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + idUsuarioActualiza;

            try
            {
                return BLL.MulData.QueryLibre(query);
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para obtener especimenes de estudio", exc);
            }
        }

        public static bool EliminarEspecimenDeEstudio(string claveEstudio, int idUsuarioActualiza)
        {
            string query =
                "EXEC [dbo].[ELIMINAR_ESPECIMEN_DE_ESTUDIO] " +
                "@clave_estudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + idUsuarioActualiza;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para eliminar especimen de estudio", exc);
            }
        }

        public static bool GuardarEstudiCompleto(string claveEstudio, string idCliente, string idPaciente, string idDoctor, string idPatologoResponsable, string fechatoma, string fechaRegistro, string idProcedencia, int idUsuarioActualiza, int idStatus, string enviarRevision, string idSubProcedencia, string edad, string fechaCompromiso)
        {
            string query =
                "EXEC [dbo].[GUARDAR_ESTUDIO_COMPLETO] " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idCliente = " + idCliente + ", " +
                "@idPaciente = " + idPaciente + ", " +
                "@idDoctor = " + idDoctor + "," +
                "@idPatologoResponsable = " + idPatologoResponsable + "," +
                "@fechaToma = '" + fechatoma + "', " +
                "@fecharegistro = '" + fechaRegistro + "', " +
                "@idProcedencia = " + idProcedencia + ", " +
                "@idUsuarioActualiza = " + idUsuarioActualiza + ", " +
                "@idStatus = " + idStatus + ", " +
                "@enviarRevision = '" + enviarRevision + "', " +
                "@idSubProcedencia = " + idSubProcedencia + ", " +
                "@edad = " + edad +", " +
                "@fechaCompromiso = '" + fechaCompromiso +"'";

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para ACTUALIZAR estudio", exc);
            }
        }

        public static DataTable ObtenerTiposEstudio(int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_TIPOS_ESTUDIO] " +
                "@idUsuarioActualiza = " + id_usuario_alta;


            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerTiposProcedencia(int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_TIPOS_PROCEDENCIA] " +
                "@idUsuarioActualiza = " + id_usuario_alta;


            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerTiposProcedenciaHijos(int idProcedenciaPadre, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_TIPOS_PROCEDENCIA_HIJOS] " +
                "@idProcedenciaPadre = " + idProcedenciaPadre + "," +
                "@idUsuarioActualiza = " + id_usuario_alta;


            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerEstudioEnProceso(int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_ESTUDIO_EN_PROCESO] " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerDatosEstudio(string idEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_DATOS_ESTUDIO] " +
                "@idEstudio = " + idEstudio + ", " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static bool EliminarEstudio(string claveEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[CANCELAR_ESTUDIO] " +
                "@claveEstudio = '" + claveEstudio + "' ," +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para cancelar estudio", exc);
            }
        }

        public static DataTable ObtenerEstudiosPorEstatus(int status, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_ESTUDIOS_POR_ESTATUS]  " +
                "@idEstatus = " + status + ", " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerEstudioQuirurgico(string claveEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_ESTUDIO_QUIRURGICO]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerEstudioCitologiaDiversos(string claveEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_ESTUDIO_CITOLOGIA_DIVERSOS]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerEstudioCitologiaTipificacion(string claveEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_ESTUDIO_CITOLOGIA_TIPIFICACION]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerEstudioRenalNativa(string claveEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_ESTUDIO_RENAL_NATIVA]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerEstudioRenalTrasnplantada(string claveEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_ESTUDIO_RENAL_TRANSPLANTADA]   " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerEstudioCitologiaCervicovaginal(string claveEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_ESTUDIO_CITOLOGIA_CERVICOVAGINAL]   " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerPlantillasEstudioQuirurgico(string claveEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_PLANTILLAS_ESTUDIO_QUIRURGICO]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable VerificarImagenEstudio(string claveEstudio, string nombreImagen, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[VERIFICAR_IMAGEN_ESTUDIO]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@nombreArchivo = '" + nombreImagen + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static bool InsertarImagenEstudio(string claveEstudio, string nombreImagen, string ruta, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[INSERTAR_IMAGEN_ESTUDIO]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@nombreArchivo = '" + nombreImagen + "', " +
                "@ruta = '" + ruta + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar imagen de estudio", exc);
            }

        }

        public static DataTable ObtenerImagenesEstudio(string claveEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_IMAGENES_ESTUDIO]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static bool InsertarResultadosEstudioQuirurgico(string claveEstudio, string resultado, /*string macroscopica, string microscopica, string diagnostico,*/ int id_usuario_alta)
        {
            string query = "EXEC [dbo].[INSERTAR_RESULTADOS_ESTUDIO_QUIRURGICO]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                //"@macroscopica = '" + macroscopica + "', " +
                //"@microscopica = '" + microscopica + "', " +
                //"@diagnostico = '" + diagnostico + "', " +
                "@resultado = '" + resultado + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar imagen de estudio", exc);
            }

        }

        public static bool InsertarResultadosEstudioCitologiaDiversos(string claveEstudio, string resultado, /*string macroscopica, string microscopica, string diagnostico,*/ int id_usuario_alta)
        {
            string query = "EXEC [dbo].[INSERTAR_RESULTADOS_ESTUDIO_CITOLOGIA_DIVERSOS]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                //"@macroscopica = '" + macroscopica + "', " +
                //"@microscopica = '" + microscopica + "', " +
                //"@diagnostico = '" + diagnostico + "', " +
                "@resultado = '" + resultado + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar imagen de estudio", exc);
            }

        }

        public static bool InsertarResultadosEstudioCitologiaTipificacion(string claveEstudio, string vph, string r1618, string resultado, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[INSERTAR_RESULTADOS_ESTUDIO_CITOLOGICO_TIPIFICACION]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@vph = '" + vph + "', " +
                "@r1618 = '" + r1618 + "', " +
                "@resultado = '" + resultado + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar imagen de estudio", exc);
            }

        }

        public static bool InsertarResultadosEstudioRenalNativo(string claveEstudio, string resultado, string lga, string lgg, string lgm, string c1q, string c3c, string fibriogeno, string kappa, string lambda, string albumina, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[INSERTAR_RESULTADOS_ESTUDIO_RENAL_NATIVA]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@resultado = '" + resultado + "', " +
                "@lga = '" + lga + "', " +
                "@lgg = '" + lgg + "', " +
                "@lgm = '" + lgm + "', " +
                "@c1q = '" + c1q + "', " +
                "@c3c = '" + c3c + "', " +
                "@fibriogeno = '" + fibriogeno + "', " +
                "@kappa = '" + kappa + "', " +
                "@lambda = '" + lambda + "', " +
                "@albumina = '" + albumina + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar estudio renal", exc);
            }

        }

        public static bool InsertarResultadosEstudioRenalTransplantada(string claveEstudio, string macroscopica, string diagnostico,string lga, string lgg, string lgm, string c1q, string c3c, string fibriogeno, string c4d, string albumina, string I, string II, string III, string IV, string V, string VI, string VII, string VIII, string IX, string X, string XI, string XII, string XIII, string XIV, string XV, string XVI, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[INSERTAR_RESULTADOS_ESTUDIO_RENAL_TRANSPLANTADA]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@macroscopica = '" + macroscopica + "', " +
                "@diagnostico = '" + diagnostico + "', " +
                "@lga = '" + lga + "', " +
                "@lgg = '" + lgg + "', " +
                "@lgm = '" + lgm + "', " +
                "@c1q = '" + c1q + "', " +
                "@c3c = '" + c3c + "', " +
                "@fibriogeno = '" + fibriogeno + "', " +
                "@c4d = '" + c4d + "', " +
                "@albumina = '" + albumina + "', " +
                "@I = '" + I + "', " +
                "@II = '" + II + "', " +
                "@III = '" + III + "', " +
                "@IV = '" + IV + "', " +
                "@V = '" + V + "', " +
                "@VI = '" + VI + "', " +
                "@VII = '" + VII + "', " +
                "@VIII = '" + VIII + "', " +
                "@IX = '" + IX + "', " +
                "@X = '" + X + "', " +
                "@XI = '" + XI + "', " +
                "@XII = '" + XII + "', " +
                "@XIII = '" + XIII + "', " +
                "@XIV = '" + XIV + "', " +
                "@XV = '" + XV + "', " +
                "@XVI = '" + XVI + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar estudio renal transplantada", exc);
            }

        }

        public static bool InsertarResultadosEstudioCitologiaCervicovaginal(string claveEstudio, string suferficiales, string intermedia, string parabasales, string celulasEndocervicales, string celulasReserva, string celulasMetaplasia, string actividadEstrogenica, string valorEstrogenico, string bacilosDordelein, string floraCocoide, string floraMixta, string candidaAlbicans, string candidaGlabrata, string trichomonaVaginalis, string gardnerella, string actinomyces, string herpesVaginalis, string virusPapilomaHumano, string disqueratosis, string citolisis, string coilocitosis, string paraqueratosis, string pseudoqueratosis, string atipiaRegenerativa, string displacia, string neoplasia, string moco, string polimorfonucleares, string linfocitos, string macrogafos, string eritrocitos,  string tipoMuestra, string calidadMuestra, string resultado, string resultadoSecundario,int id_usuario_alta, string comentarios)
        {
            string query = "EXEC [dbo].[INSERTAR_RESULTADOS_ESTUDIO_CITOLOGIA_CERVICOVAGINAL]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@superficiales = '" + suferficiales + "', " +
                "@intermedia = '" + intermedia + "', " +
                "@parabasales = '" + parabasales + "', " +
                "@celulasEndocervicales = '" + celulasEndocervicales + "', " +
                "@celulasReserva = '" + celulasReserva + "', " +
                "@celulasMetaplasia = '" + celulasMetaplasia + "', " +
                "@actividadEstrogenica = '" + actividadEstrogenica + "', " +
                "@valorEstrogenico = '" + valorEstrogenico + "', " +
                "@bacilosDoderlein = '" + bacilosDordelein + "', " +
                "@floraCocoide = '" + floraCocoide + "', " +
                "@floraMixta = '" + floraMixta + "', " +
                "@candidaAlbicans = '" + candidaAlbicans + "', " +
                "@candidaGlabrata = '" + candidaGlabrata + "', " +
                "@trichomonaVaginalis = '" + trichomonaVaginalis + "', " +
                "@gardnerella = '" + gardnerella + "', " +
                "@actinomyces = '" + actinomyces + "', " +
                "@herpesVaginalis = '" + herpesVaginalis + "', " +
                "@virusPapilomaHumnano = '" + @virusPapilomaHumano + "', " +
                "@disqueratosis = '" + disqueratosis + "', " +
                "@citolisis = '" + citolisis + "', " +
                "@coilocitosis = '" + coilocitosis + "', " +
                "@paraqueratosis = '" + paraqueratosis + "', " +
                "@pseudoqueratosis = '" + pseudoqueratosis + "', " +
                "@atipiaRegenerativa = '" + atipiaRegenerativa + "', " +
                "@displacia = '" + displacia + "', " +
                "@neoplasia = '" + neoplasia + "', " +
                "@moco = '" + moco + "', " +
                "@polimorfonucleares = '" + polimorfonucleares + "', " +
                "@linfocitos = '" + linfocitos + "', " +
                "@macrofagos = '" + macrogafos + "', " +
                "@eritocitos = '" + eritrocitos + "', " +
                "@tipoMuestra = '" + tipoMuestra + "', " +
                "@calidadMuestra = '" + calidadMuestra + "', " +
                "@resultado = '" + resultado + "', " +
                "@resultadoSecundario = " + resultadoSecundario + ", " +
                "@idUsuarioActualiza = " + id_usuario_alta +", " +
                "@comentarios =" + comentarios;

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar resultado estudio citoloico cervicovaginal", exc);
            }

        }

        public static bool ActualizarResultadosEstudioQuirurgico(string claveEstudio, string resultado, /*string macroscopica, string microscopica, string diagnostico,*/ int id_usuario_alta, string actualizarEstatus)
        {
            string query = "EXEC [dbo].[ACTUALIZAR_RESULTADOS_ESTUDIO_QUIRURGICO]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                /*"@macroscopica = '" + macroscopica + "', " +
                "@microscopica = '" + microscopica + "', " +
                "@diagnostico = '" + diagnostico + "', " +*/
                "@resultado = '"+resultado+"',"+
                "@idUsuarioActualiza = " + id_usuario_alta + ", "+
                "@actualizarEstatus = '" + actualizarEstatus + "'";

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar imagen de estudio", exc);
            }

        }

        public static bool ActualizarResultadosEstudioCitologiaDiversos(string claveEstudio, string resultado, /*string macroscopica, string microscopica, string diagnostico,*/ int id_usuario_alta, string actualizarEstatus)
        {
            string query = "EXEC [dbo].[ACTUALIZAR_RESULTADOS_ESTUDIO_CITOLOGIA_DIVERSOS]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                /*"@macroscopica = '" + macroscopica + "', " +
               "@microscopica = '" + microscopica + "', " +
               "@diagnostico = '" + diagnostico + "', " +*/
                "@resultado = '" + resultado + "'," +
                 "@idUsuarioActualiza = " + id_usuario_alta + ", " +
                "@actualizarEstatus = '" + actualizarEstatus + "'";

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar imagen de estudio", exc);
            }

        }

        public static bool ActualizarResultadosEstudioCitologiaTipificacion(string claveEstudio, string vph, string r1618, string resultado, int id_usuario_alta, string actualizarEstatus)
        {
            string query = "EXEC [dbo].[ACTUALIZAR_RESULTADOS_ESTUDIO_CITOLOGIA_TIPIFICACION]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@vph = '" + vph + "', " +
                "@r1618 = '" + r1618 + "', " +
                "@resultado = '" + resultado + "', " +
                 "@idUsuarioActualiza = " + id_usuario_alta + ", " +
                "@actualizarEstatus = '" + actualizarEstatus + "'";

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar imagen de estudio", exc);
            }

        }

        public static bool ActualizarResultadosEstudioRenalNativa(string claveEstudio, string resultado,string lga, string lgg, string lgm, string c1q, string c3c, string fibriogeno, string kappa, string lambda, string albumina, int id_usuario_alta, string actualizarEstatus)
        {
            string query = "EXEC [dbo].[ACTUALIZAR_RESULTADOS_ESTUDIO_RENAL_NATIVA]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@resultado = '" + resultado + "', " +
                "@lga = '" + lga + "', " +
                "@lgg = '" + lgg + "', " +
                "@lgm = '" + lgm + "', " +
                "@c1q = '" + c1q + "', " +
                "@c3c = '" + c3c + "', " +
                "@fibriogeno = '" + fibriogeno + "', " +
                "@kappa = '" + kappa + "', " +
                "@lambda = '" + lambda + "', " +
                "@albumina = '" + albumina + "', " +
                 "@idUsuarioActualiza = " + id_usuario_alta + ", " +
                "@actualizarEstatus = '" + actualizarEstatus + "'";

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar imagen de estudio", exc);
            }

        }

        public static bool ActualizarResultadosEstudioRenalTransplantada(string claveEstudio, string macroscopica, string diagnostico,string lga, string lgg, string lgm, string c1q, string c3c, string fibriogeno, string c4d, string albumina, string I, string II, string III, string IV, string V, string VI, string VII, string VIII, string IX, string X, string XI, string XII, string XIII, string XIV, string XV, string XVI, int id_usuario_alta, string actualizarEstatus)
        {
            string query = "EXEC [dbo].[ACTUALIZAR_RESULTADOS_ESTUDIO_RENAL_TRANSPLANTADA]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@macroscopica = '" + macroscopica + "', " +
                "@diagnostico = '" + diagnostico + "', " +
                "@lga = '" + lga + "', " +
                "@lgg = '" + lgg + "', " +
                "@lgm = '" + lgm + "', " +
                "@c1q = '" + c1q + "', " +
                "@c3c = '" + c3c + "', " +
                "@fibriogeno = '" + fibriogeno + "', " +
                "@c4d = '" + c4d + "', " +
                "@albumina = '" + albumina + "', " +
                "@I = '" + I + "', " +
                "@II = '" + II + "', " +
                "@III = '" + III + "', " +
                "@IV = '" + IV + "', " +
                "@V = '" + V + "', " +
                "@VI = '" + VI + "', " +
                "@VII = '" + VII + "', " +
                "@VIII = '" + VIII + "', " +
                "@IX = '" + IX + "', " +
                "@X = '" + X + "', " +
                "@XI = '" + XI + "', " +
                "@XII = '" + XII + "', " +
                "@XIII = '" + XIII + "', " +
                "@XIV = '" + XIV + "', " +
                "@XV = '" + XV + "', " +
                "@XVI = '" + XVI + "', " +
                 "@idUsuarioActualiza = " + id_usuario_alta + ", " +
                "@actualizarEstatus = '" + actualizarEstatus + "'";

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para actualizar estudio renal trasnplantado", exc);
            }

        }

        public static bool ActualizarResultadosEstudioCitologiaCervicovaginal(string claveEstudio, string suferficiales, string intermedia, string parabasales, string celulasEndocervicales, string celulasReserva, string celulasMetaplasia, string actividadEstrogenica, string valorEstrogenico, string bacilosDordelein, string floraCocoide, string floraMixta, string candidaAlbicans, string candidaGlabrata, string trichomonaVaginalis, string gardnerella, string actinomyces, string herpesVaginalis, string virusPapilomaHumano, string disqueratosis, string citolisis, string coilocitosis, string paraqueratosis, string pseudoqueratosis, string atipiaRegenerativa, string displacia, string neoplasia, string moco, string polimorfonucleares, string linfocitos, string macrogafos, string eritrocitos, string tipoMuestra, string calidadMuestra, string resultado, string resultadoSecundario, int id_usuario_alta, string actualizarEstatus, string comentarios)
        {
            string query = "EXEC [dbo].[ACTUALIZAR_RESULTADOS_ESTUDIO_CITOLOGIA_CERVICOVAGINAL]  " +
               "@claveEstudio = '" + claveEstudio + "', " +
                "@superficiales = '" + suferficiales + "', " +
                "@intermedia = '" + intermedia + "', " +
                "@parabasales = '" + parabasales + "', " +
                "@celulasEndocervicales = '" + celulasEndocervicales + "', " +
                "@celulasReserva = '" + celulasReserva + "', " +
                "@celulasMetaplasia = '" + celulasMetaplasia + "', " +
                "@actividadEstrogenica = '" + actividadEstrogenica + "', " +
                "@valorEstrogenico = '" + valorEstrogenico + "', " +
                "@bacilosDoderlein = '" + bacilosDordelein + "', " +
                "@floraCocoide = '" + floraCocoide + "', " +
                "@floraMixta = '" + floraMixta + "', " +
                "@candidaAlbicans = '" + candidaAlbicans + "', " +
                "@candidaGlabrata = '" + candidaGlabrata + "', " +
                "@trichomonaVaginalis = '" + trichomonaVaginalis + "', " +
                "@gardnerella = '" + gardnerella + "', " +
                "@actinomyces = '" + actinomyces + "', " +
                "@herpesVaginalis = '" + herpesVaginalis + "', " +
                "@virusPapilomaHumnano = '" + @virusPapilomaHumano + "', " +
                "@disqueratosis = '" + disqueratosis + "', " +
                "@citolisis = '" + citolisis + "', " +
                "@coilocitosis = '" + coilocitosis + "', " +
                "@paraqueratosis = '" + paraqueratosis + "', " +
                "@pseudoqueratosis = '" + pseudoqueratosis + "', " +
                "@atipiaRegenerativa = '" + atipiaRegenerativa + "', " +
                "@displacia = '" + displacia + "', " +
                "@neoplasia = '" + neoplasia + "', " +
                "@moco = '" + moco + "', " +
                "@polimorfonucleares = '" + polimorfonucleares + "', " +
                "@linfocitos = '" + linfocitos + "', " +
                "@macrofagos = '" + macrogafos + "', " +
                "@eritocitos = '" + eritrocitos + "', " +
                "@tipoMuestra = '" + tipoMuestra + "', " +
                "@calidadMuestra = '" + calidadMuestra + "', " +
                "@resultado = '" + resultado + "', " +
                "@resultadoSecundario = " + resultadoSecundario + ", " +
                "@idUsuarioActualiza = " + id_usuario_alta + ", " +
                "@actualizarEstatus = '" + actualizarEstatus + "'," +
                "@comentarios = " + comentarios;

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para insertar resultado estudio citoloico cervicovaginal", exc);
            }

        }


        public static int ObtenerCuentaImagenesEstudio(string claveEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_CUENTA_IMAGENES_ESTUDIO]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return Convert.ToInt32(data.Rows[0]["CUENTA"]);
        }

        public static int ObtenerEstatusEstudio(string idEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_STATUS_ESTUDIO]  " +
                "@idStudio = '" + idEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return Convert.ToInt32(data.Rows[0]["ID_STATUS"]);
        }

        public static bool ActualizarEspecimenEstudio(string claveEspecimen, string precioVenta, string cantidad, int id_usuario_alta, int cantidadEtiquetas)
        {
            string query = "EXEC [dbo].[ACTUALIZAR_ESPECIMEN_ESTUDIO]  " +
                "@idEspecimen = '" + claveEspecimen + "', " +
                "@precio = " + precioVenta + ", " +
                "@cantidad = " + cantidad + ", " +
                "@cantidadEtiquetas = " + cantidadEtiquetas + ", " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para actualizar especimen estudio, exc");
            }
        }

        public static bool ActualizarEstatusEstudio(int idEstudio, int estatus, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[ACTUALIZAR_ESTATUS_ESTUDIO]  " +
                "@idEstudios = " + idEstudio + ", " +
                "@idEstatus = " + estatus + ", " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            try
            {
                BLL.MulData.QueryLibre2(query);

                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para actualizar estatus estudio, exc");
            }
        }

        public static int ObtenerTipoEstudio(string claveEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_TIPO_ESTUDIO]  " +
                "@claveEstudio = '" + claveEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return Convert.ToInt32(data.Rows[0]["ID_TIPO_ESTUDIO"]);
        }

        public static string ObtenerTipoReporte(string idEstudio, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_TIPO_REPORTE]  " +
                "@idEstudio = '" + idEstudio + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data.Rows[0]["NOMBRE_REPORTE"].ToString();
        }

        public static string ObtenerTipoReporteClave(string clave, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_TIPO_REPORTE_CLAVE]  " +
                "@claveEstudio = '" + clave + "', " +
                "@idUsuarioActualiza = " + id_usuario_alta;

            DataTable data = BLL.MulData.QueryLibre(query);

            return data.Rows[0]["NOMBRE_REPORTE"].ToString();
        }

    }
}

